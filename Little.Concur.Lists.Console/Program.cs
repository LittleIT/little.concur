﻿using System;
using System.IO;
using Little.Concur.Lists.Core;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace Little.Concur.Lists
{
    class Program
    {
        public const string AppName = "LittleConcurLists";

        static void Main(string[] args)
        {
            Console.Title = AppName;
            try
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");
                var config = builder.Build();
                var settings = new ConcurSettings();
                config.GetSection("ConcurSettings").Bind(settings);
                string dataConnection = config.GetConnectionString($"{settings.Environment}Connection");
                string loggingConnection = config.GetConnectionString("LoggingConnection");
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .Enrich.WithMachineName()
                    .Enrich.WithEnvironmentUserName()
                    .Enrich.WithProcessName()
                    .WriteTo.Debug()
                    .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
                    .WriteTo.MSSqlServer(loggingConnection, "Logs", LogEventLevel.Information)
                    .CreateLogger();

                var app = new CommandLineApplication();
                app.Name = AppName;
                app.HelpOption("-?|-h|--help");
                app.OnExecute(() =>
                {
                    try
                    {
                        var mgr = new ConcurListIntegrator(settings, dataConnection);
                        mgr.Execute();
                    }
                    catch (Exception ex)
                    {
                        var log = Log.ForContext<ConcurListIntegrator>();
                        log.Error(ex, ex.Message);
                    }
                    finally
                    {
                        Log.CloseAndFlush();
                    }
                    return 0;
                });
                app.Execute(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fatal error: unable to configure or setup application.");
                Console.WriteLine(ex.Message);
            }
        }
    }
}