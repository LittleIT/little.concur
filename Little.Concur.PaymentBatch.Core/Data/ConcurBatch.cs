﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Little.Concur.PaymentBatch.Core.Data
{
    /// <summary>
    /// CSV file import. Changing/adding/removing properties will blow up the importer, unless a separate map class is used.
    /// </summary>
    public class ConcurBatch
    {
        public DateTime DATE { get; set; }
        public string EmployeeID { get; set; } 
        public string EmployeeLastName { get; set; }
        public string EmployeeFirstName { get; set; }
        public string ReportID { get; set; }
        public string EmployeeDefaultCurrencyAlphaCode { get; set; }
        public DateTime ReportSubmitDate { get; set; }
        public DateTime ReportProcessingPaymentDate { get; set; }
        public string ReportName { get; set; }
        public string ReportEntryExpenseTypeName { get; set; }
        public DateTime ReportEntryTransactionDate { get; set; }
        public string ReportEntryIsPersonalFlag { get; set; } // Y/N
        public string ReportEntryDescription { get; set; }
        public string ReportEntryVendorName { get; set; }
        public string ReportEntryVendorDescription { get; set; }
        public string ReportEntryPaymentCodeCode { get; set; } // Double "CodeCode" name is in actual CSV. We'll keep it to avoid separate mapping class.
        public string ReportEntryPaymentCodeName { get; set; }
        public string ReportPaymentReimbursementType { get; set; }
        public string BilledCreditCardAccountNumber { get; set; }
        public string BilledCreditCardAccountDescription { get; set; }
        public string JournalPayerPaymentCodeName { get; set; }
        public string JournalPayeePaymentTypeName { get; set; }
        public decimal JournalAmount { get; set; }
        public string JournalAccountCode { get; set; }
        public string JournalDebitOrCredit { get; set; }
        public string PaymentDemandCompanyCashAccountCode { get; set; }
        public string PaymentDemandCompanyLiabilityAccountCode { get; set; }
        public DateTime EstimatedPaymentDate { get; set; }
        public string Project { get; set; }
        public string Phase { get; set; }
        public string Task { get; set; }
        public string Studio { get; set; }

        /// <summary>
        /// Primary entry
        /// </summary>
        public StagingJournalEntry ToJournalEntry(List<BstEmployee> employees, List<DateTime> periods)
        {
            var lastDay = this.ReportEntryTransactionDate.AddMonths(1);
            lastDay = lastDay.AddDays(lastDay.Day * -1);
            return new StagingJournalEntry
            {
                StageId = GuidUtilities.GetNextGuid(),
                trn_co_code = "00",
                input_date = this.ReportProcessingPaymentDate,
                //doc_date = this.ReportProcessingPaymentDate,
                doc_date = periods.Where(x => x < this.ReportProcessingPaymentDate).OrderByDescending(x => x).First(),
                trn_date = this.ReportEntryTransactionDate,
                per_end_date = periods.Where(x => x < this.ReportProcessingPaymentDate).OrderByDescending(x => x).First(),
                extern_ref_date = lastDay,
                acct_code = this.JournalAccountCode,
                db_amt_ac = this.JournalDebitOrCredit == "DR" ? this.JournalAmount : 0m,
                cr_amt_ac = this.JournalDebitOrCredit == "CR" ? this.JournalAmount : 0m,
                db_amt_cc = this.JournalDebitOrCredit == "DR" ? this.JournalAmount : 0m,
                cr_amt_cc = this.JournalDebitOrCredit == "CR" ? this.JournalAmount : 0m,

                //Note: during further testing we discovered that Concur is using the string "CASH" instead of "CBEP"
                //All subsequent instances of "CBEP" are commented out and replaced with "CASH"

                //evc_type_code = this.ReportEntryPaymentCodeCode == "CBEP" ? "E" : "V",
                //evc_code = this.ReportEntryPaymentCodeCode == "CBEP" ? this.EmployeeID : "BOFACC",
                evc_type_code = this.ReportEntryPaymentCodeCode == "CASH" ? "E" : "V",
                evc_code = this.ReportEntryPaymentCodeCode == "CASH" ? this.EmployeeID : "BOFACC",

                org_code = this.Studio,
                prj_code = Project,
                phase_code = Phase,
                task_code = Task,

                //Accounting requests that the pcs_desc field contain a concatenation of the report ID and the user's name, using a pipe as delimeter
                pcs_desc = string.Concat(this.ReportID.ToString(), "|", employees.First(x => x.Code == this.EmployeeID).Name),
                //pcs_desc = employees.First(x => x.Code == this.EmployeeID).Name, // Docs mention "Employee's Last Name + Employee's First Name"

                intern_ref_nbr = this.ReportID.Substring(0, 10) // Docs mention "this field's character max is 10, therefore may be able to only pull in last/first 10 characters of Report ID".
            };
        }

        /// <summary>
        /// Basically the opposite entry to balance out accounts
        /// </summary>
        public StagingJournalEntry ToJournalEntryPayment(List<BstEmployee> employees, List<DateTime> periods)
        {
            var lastDay = this.ReportEntryTransactionDate.AddMonths(1);
            lastDay = lastDay.AddDays(lastDay.Day * -1);
            return new StagingJournalEntry
            {
                StageId = GuidUtilities.GetNextGuid(),
                trn_co_code = "00",
                input_date = this.ReportProcessingPaymentDate,
                //doc_date = this.ReportProcessingPaymentDate,
                doc_date = periods.Where(x => x < this.ReportProcessingPaymentDate).OrderByDescending(x => x).First(),
                trn_date = this.ReportEntryTransactionDate,
                per_end_date = periods.Where(x => x < this.ReportProcessingPaymentDate).OrderByDescending(x => x).First(),
                extern_ref_date = lastDay,
                
                //Note: during further testing we discovered that Concur is using the string "CASH" instead of "CBEP"
                //All subsequent instances of "CBEP" are commented out and replaced with "CASH"
                
                //acct_code = this.ReportEntryPaymentCodeCode == "CBEP" ? "1018" : "8001",                
                acct_code = this.ReportEntryPaymentCodeCode == "CASH" ? "1018" : "8001",
                db_amt_ac = this.JournalDebitOrCredit == "CR" ? this.JournalAmount : 0m,
                cr_amt_ac = this.JournalDebitOrCredit == "DR" ? this.JournalAmount : 0m,
                db_amt_cc = this.JournalDebitOrCredit == "CR" ? this.JournalAmount : 0m,
                cr_amt_cc = this.JournalDebitOrCredit == "DR" ? this.JournalAmount : 0m,
                //evc_type_code = this.ReportEntryPaymentCodeCode == "CBEP" ? "E" : "V",
                evc_type_code = this.ReportEntryPaymentCodeCode == "CASH" ? "E" : "V",
                //evc_code = this.ReportEntryPaymentCodeCode == "CBEP" ? this.EmployeeID : "BOFACC",
                evc_code = this.ReportEntryPaymentCodeCode == "CASH" ? this.EmployeeID : "BOFACC",

                //Set work breakdown schedule indicator. Set to NO on *both* company and employee paid expenses
                //wbs_ind = (this.ReportEntryPaymentCodeCode == "CBEP" || this.ReportEntryPaymentCodeCode == "CBCP") ? "N" : "Y",
                wbs_ind = (this.ReportEntryPaymentCodeCode == "CASH" || this.ReportEntryPaymentCodeCode == "CBCP") ? "N" : "Y",

                //TODO: Kevin, there is where I believe the project, phase, task nned to be set for the reversal                
                //if employee paid (CASH) then org code is 00000; if company paid (CBCP) then org code is the employee's home org                
                org_code = this.ReportEntryPaymentCodeCode == "CASH" ? "00000" : employees.First(x => x.Code == this.EmployeeID).OrgCode,

                //Per Accounting, the project/phase/task are to remain unpopulated for *both* company and employee paid expenses
                //prj_code = this.ReportEntryPaymentCodeCode == "CBEP" ? Project : null,
                //phase_code = this.ReportEntryPaymentCodeCode == "CBEP" ? Phase : null,
                //task_code = this.ReportEntryPaymentCodeCode == "CBEP" ? Task : null,
                //prj_code = (this.ReportEntryPaymentCodeCode == "CBEP" || this.ReportEntryPaymentCodeCode == "CBCP") ? null : Project,
                //phase_code = (this.ReportEntryPaymentCodeCode == "CBEP" || this.ReportEntryPaymentCodeCode == "CBCP") ? null : Phase,
                //task_code = (this.ReportEntryPaymentCodeCode == "CBEP" || this.ReportEntryPaymentCodeCode == "CBCP") ? null : Task,

                prj_code = (this.ReportEntryPaymentCodeCode == "CASH" || this.ReportEntryPaymentCodeCode == "CBCP") ? null : Project,
                phase_code = (this.ReportEntryPaymentCodeCode == "CASH" || this.ReportEntryPaymentCodeCode == "CBCP") ? null : Phase,
                task_code = (this.ReportEntryPaymentCodeCode == "CASH" || this.ReportEntryPaymentCodeCode == "CBCP") ? null : Task,

                //Accounting requests that the pcs_desc field contain a concatenation of the report ID and the user's name, using a pipe as delimeter
                pcs_desc = string.Concat(this.ReportID.ToString(), "|", employees.First(x => x.Code == this.EmployeeID).Name),
                //pcs_desc = employees.First(x => x.Code == this.EmployeeID).Name, // Docs mention "Employee's Last Name + Employee's First Name"
                intern_ref_nbr = this.ReportID.Substring(0, 10) // Docs mention "this field's character max is 10, therefore may be able to only pull in last/first 10 characters of Report ID".
            };
        }
    }
}