﻿using System;

namespace Little.Concur.PaymentBatch.Core.Data
{
    public class BstPeriodEndDate
    {
        public DateTime per_end_date { get; set; }
        public short fiscal_year { get; set; }
        public short fiscal_per_nbr { get; set; }
        public int accounting_period { get; set; }
        public DateTime mod_date { get; set; }
        public short mod_nbr { get; set; }
        public int mod_usr_skey { get; set; }
        public string rec_status_ind { get; set; }
        public string labor_period_yn { get; set; }
        public string expense_period_yn { get; set; }
    }
}