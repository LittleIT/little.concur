﻿namespace Little.Concur.PaymentBatch.Core.Data
{
    public class BstEmployee
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string OrgCode { get; set; }
    }
}