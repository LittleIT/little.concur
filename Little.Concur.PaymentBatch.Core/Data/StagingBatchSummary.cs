﻿using System;
using System.Collections.Generic;

namespace Little.Concur.PaymentBatch.Core.Data
{
    public class StagingBatchSummary : IEquatable<StagingBatchSummary>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public int Count { get; set; }
        public bool IsClosed { get; set; }
        public string Type { get; set; }
        public string PaymentMethod { get; set; }
        public string BatchUrl { get; set; }
        public DateTimeOffset Retrieved { get; set; }

        public int StagedJournalEntryCount { get; set; }
        public int CompletedJournalEntryCount { get; set; }

        public List<StagingJournalEntry> StagedEntries { get; set; }

        public bool IsFileLocal { get; set; }
        public string FileName { get { return $"{Id}.csv"; } }

        public string BatchUrlPathAndQuery {
            get {
                if (string.IsNullOrWhiteSpace(BatchUrl))
                {
                    return null;
                }
                return new Uri(BatchUrl).PathAndQuery;
            }
        }

        /// <summary>
        /// Required for retrieving batch file via API
        /// </summary>
        public string EncryptedId {
            get {
                if (string.IsNullOrWhiteSpace(BatchUrl))
                {
                    return null;
                }
                return BatchUrl.Substring(BatchUrl.LastIndexOf('/') + 1);
            }
        }

        public bool Equals(StagingBatchSummary other)
        {
            if (other is null)
            {
                return false;
            }
            if (Object.ReferenceEquals(this, other))
            {
                return true;
            }
            return Id == other.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }

    /// <summary>
    /// Comparer for batch values. Ignores retrieved data and calculated fields.
    /// </summary>
    public class StagingBatchSummaryComparer : IEqualityComparer<StagingBatchSummary>
    {
        public bool Equals(StagingBatchSummary x, StagingBatchSummary y)
        {
            if (x is null || y is null)
            {
                return false;
            }
            if (Object.ReferenceEquals(x, y))
            {
                return true;
            }
            return x.Id == y.Id
                && x.Name.SafeEquals(y.Name)
                && x.Amount == y.Amount
                && x.Count == y.Count
                && x.IsClosed == y.IsClosed
                && x.Type.SafeEquals(y.Type)
                && x.PaymentMethod.SafeEquals(y.PaymentMethod)
                && x.BatchUrl.SafeEquals(y.BatchUrl);
        }

        public int GetHashCode(StagingBatchSummary obj)
        {
            return obj.Id.GetHashCode() ^ obj.Name.SafeHashCode() ^ obj.Amount.GetHashCode() ^ obj.Count.GetHashCode() ^ obj.IsClosed.GetHashCode()
                ^ obj.Type.SafeHashCode() ^ obj.PaymentMethod.SafeHashCode() ^ obj.BatchUrl.SafeHashCode();
        }
    }
}