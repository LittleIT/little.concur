﻿using System;

namespace Little.Concur.PaymentBatch.Core.Data
{
    public class StagingJournalEntry : BstJournalEntryInterface, IEquatable<StagingJournalEntry>
    {
        public string ConcurBatchID { get; set; }
        public Guid StageId { get; set; }
        public DateTimeOffset Staged { get; set; }
        public DateTimeOffset? Completed { get; set; }

        public bool Equals(StagingJournalEntry other)
        {
            if (other is null)
            {
                return false;
            }
            if (Object.ReferenceEquals(this, other))
            {
                return true;
            }
            return batch_skey == other.batch_skey
                && seq_nbr == other.seq_nbr
                && line_nbr == other.line_nbr;
        }

        public override int GetHashCode()
        {
            return batch_skey.GetHashCode() ^ seq_nbr.GetHashCode() ^ line_nbr.GetHashCode();
        }
    }
}