﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Little.Concur.PaymentBatch.Core.Data
{
    public class BatchSummary : IEquatable<BatchSummary>
    {
        // Raw result from Concur
        public string BatchID { get; set; }
        public string BatchName { get; set; }
        public string BatchTotal { get; set; }
        public string Currency { get; set; }
        public string Count { get; set; }
        public string Type { get; set; }
        public string PaymentMethod { get; set; }
        public string BatchUrl { get; set; }

        // Database
        public DateTimeOffset Retrieved { get; set; }
        public decimal BatchTotalAmount { get; set; }

        // Helper properties utilized by application logic
        public bool IsClosed { get; set; }
        public bool IsFileLocal { get; set; }
        public string FileName { get { return $"{BatchID}.csv"; } }

        public string BatchUrlPathAndQuery {
            get {
                if (string.IsNullOrWhiteSpace(BatchUrl))
                {
                    return null;
                }
                return new Uri(BatchUrl).PathAndQuery;
            }
        }

        /// <summary>
        /// Required for retrieving batch file via API
        /// </summary>
        public string EncryptedId {
            get {
                if (string.IsNullOrWhiteSpace(BatchUrl))
                {
                    return null;
                }
                return BatchUrl.Substring(BatchUrl.LastIndexOf('/') + 1);
            }
        }

        public bool Equals(BatchSummary other)
        {
            if (other is null)
            {
                return false;
            }
            if (Object.ReferenceEquals(this, other))
            {
                return true;
            }
            return BatchID == other.BatchID;
        }

        public override int GetHashCode()
        {
            return BatchID.GetHashCode();
        }

        public StagingBatchSummary ToStagingBatchSummary(string batchFolderPath, string status, DateTimeOffset retrieved)
        {
            return new StagingBatchSummary
            {
                Id = BatchID,
                Name = BatchName.SafeTrim(),
                Amount = Decimal.TryParse(BatchTotal, out decimal a) ? a : 0m,
                Count = Int32.TryParse(Count, out int c) ? c : 0,
                IsClosed = status == "CLOSED",
                Type = Type,
                PaymentMethod = PaymentMethod,
                BatchUrl = BatchUrl,
                IsFileLocal = File.Exists(Path.Combine(batchFolderPath, FileName)),
                Retrieved = retrieved
            };
        }
    }
}