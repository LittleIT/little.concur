﻿using System;

namespace Little.Concur.PaymentBatch.Core.Data
{
    /// <summary>
    /// BST je_interface table
    /// </summary>
    public class BstJournalEntryInterface : IEquatable<BstJournalEntryInterface>
    {
        public string batch_skey { get; set; } = "1";
        public string co_code { get; set; } = "00";
        public string usr_batch_id { get; set; } = "CNC"; // They asked for CNCR, but it's a varchar(3)
        public DateTime? per_end_date { get; set; }
        public string sys_doc_type_code { get; set; } = "J"; // Journal
        public string usr_doc_type_suf { get; set; } = "UP"; // Journal Upload
        public short? max_dcml_places { get; set; } = 2;
        public DateTime? input_date { get; set; }
        public short? batch_doc_total { get; set; }
        public decimal? batch_acct_db_total { get; set; }
        public decimal? batch_acct_cr_total { get; set; }
        public decimal? batch_co_db_total { get; set; }  // No longer used?
        public decimal? batch_co_cr_total { get; set; }  // No longer used?
        public int seq_nbr { get; set; } // one-based; document sequence
        public string doc_nbr { get; set; }
        public DateTime? doc_date { get; set; }
        public string unit_input_yn { get; set; } = "N";
        public string currency_code { get; set; } = "US";
        public decimal? currency_rate { get; set; } = 1m;
        public string reversal_yn { get; set; } = "N";
        public DateTime? reversal_per_end_date { get; set; }
        public decimal? doc_acct_db_total { get; set; }
        public decimal? doc_acct_cr_total { get; set; }
        public decimal? doc_co_db_total { get; set; }  // No longer used?
        public decimal? doc_co_cr_total { get; set; }  // No longer used?
        public int line_nbr { get; set; }
        public string trn_co_code { get; set; } // 00? Review existing JE
        public string org_code { get; set; }
        public string acct_code { get; set; }
        public string evc_type_code { get; set; }
        public string evc_code { get; set; }
        public string extern_ref_nbr { get; set; } = null;
        public DateTime? extern_ref_date { get; set; } = null;
        public string intern_ref_nbr { get; set; }
        public DateTime? trn_date { get; set; }
        public string wbs_ind { get; set; } = "Y";
        public decimal? db_amt_ac { get; set; }
        public decimal? cr_amt_ac { get; set; }
        public decimal? db_amt_cc { get; set; } = null; // No longer used according to BST view
        public decimal? cr_amt_cc { get; set; } = null; // No longer used according to BST view
        public string prj_code { get; set; }
        public string phase_code { get; set; }
        public string task_code { get; set; }
        public string inv_grouping_code { get; set; } = "**"; // Accounting indicated that "**" can be used for all entries
        public string actv_code { get; set; } = "****";
        public string rsrc_code { get; set; } = "*****";
        public decimal? qty_amt { get; set; }
        public string pcs_desc { get; set; }
        public string fcs_desc { get; set; }

        public bool Equals(BstJournalEntryInterface other)
        {
            if (other is null)
            {
                return false;
            }
            if (Object.ReferenceEquals(this, other))
            {
                return true;
            }
            return batch_skey == other.batch_skey
                && seq_nbr == other.seq_nbr
                && line_nbr == other.line_nbr;
        }

        public override int GetHashCode()
        {
            return batch_skey.GetHashCode() ^ seq_nbr.GetHashCode() ^ line_nbr.GetHashCode();
        }
    }
}