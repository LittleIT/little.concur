﻿namespace Little.Concur.PaymentBatch.Core.Data
{
    public class ConcurCloseBatchResponse
    {
        public string BatchStatus { get; set; }

        /// <summary>
        /// A URL for retrieving the extract file or files produced when the batch closes, with encrypted ID.
        /// </summary>
        public string FileUrl { get; set; }
        public string Status { get; set; }
        public string JobQueueKey { get; set; }
    }
}