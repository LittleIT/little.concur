﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CsvHelper;
using Dapper;
using Little.Concur.PaymentBatch.Core.Data;
using Little.Concur.PaymentBatch.Core.Exceptions;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Serilog;

namespace Little.Concur.PaymentBatch.Core
{
    /// <summary>
    /// Import payment batch data from the source Concur APIs to the BST system via interface tables
    /// </summary>
    public class ConcurPaymentBatchIntegrator : IConcurPaymentBatchIntegrator
    {
        private const string TokenFileName = "api.tkn";

        private readonly ConcurBatchSettings _settings;
        private readonly IConcurTokenManager _tokenManager;
        private readonly string _connection;
        private readonly ILogger _log;
        private readonly RestClient _client;

        public ConcurPaymentBatchIntegrator(IConfiguration config, IConcurSettings settings, IConcurTokenManager tokenManager)
        {
            _settings = (ConcurBatchSettings)settings;
            _connection = config.GetConnectionString($"{_settings.Environment}Connection");
            _tokenManager = tokenManager;
            _log = Log.ForContext<ConcurPaymentBatchIntegrator>();
            if (!_settings.IsDryRun || !string.IsNullOrWhiteSpace(_settings.ConcurUrl))
            {
                _client = new RestClient(_settings.ConcurUrl);
            }
        }

        // <summary>
        /// Retrieve batch list from Concur API
        /// </summary>
        /// <param name="status">Statuses to query. Can be OPEN or CLOSED.</param>
        private async Task<List<StagingBatchSummary>> GetBatchListAsync(string status = "OPEN")
        {
            _log.Debug("Retrieving batch lists from Concur API");
            _client.Authenticator = _tokenManager.GetAuthenticator(_client);
            var request = new RestRequest($"/api/expense/paymentbatch/v1.1/batchlist/{status}", Method.GET);
            request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
            var response = await _client.ExecuteTaskAsync<List<BatchSummary>>(request);
            if (ConcurApiHelper.HasRestError(_log, response) || response.Data == null)
            {
                return null;
            }
            var retrieved = DateTimeOffset.Now;
            var data = new List<StagingBatchSummary>();
            foreach (var d in response.Data)
            {
                data.Add(d.ToStagingBatchSummary(_settings.BatchFolderPath, status, retrieved));
            }
            return data;
        }

        /// <summary>
        /// Refreshes high level staging table for batches via Concur API. Includes both open and closed batches.
        /// </summary>
        public async Task RefreshStagingBatchSummaryAsync()
        {
            var open = await GetBatchListAsync("OPEN");
            var closed = await GetBatchListAsync("CLOSED");
            var dataConcur = open.Concat(closed).ToList();
            using (var con = new SqlConnection(_connection))
            {
                await con.OpenAsync();
                using (var tx = con.BeginTransaction(System.Data.IsolationLevel.ReadCommitted, "BatchSummarySave"))
                {
                    string sql = "";
                    try
                    {
                        sql = "select * from StagingBatchSummary";
                        var dbData = await con.QueryAsync<StagingBatchSummary>(sql, null, tx);
                        var newData = dataConcur.Except(dbData);
                        if (newData.Any())
                        {
                            sql = @"insert StagingBatchSummary ([Id], [Name], [Amount], [Count], [IsClosed], [Type], [PaymentMethod], [BatchUrl], [Retrieved])
                                values (@Id, @Name, @Amount, @Count, @IsClosed, @Type, @PaymentMethod, @BatchUrl, @Retrieved)";
                            await con.ExecuteAsync(sql, newData, tx);
                        }
                        var removedData = dbData.Except(dataConcur);
                        if (removedData.Any())
                        {
                            sql = "delete StagingBatchSummary where [Id] = @Id";
                            await con.ExecuteAsync(sql, removedData, tx);
                        }
                        var modifiedData = dataConcur.Intersect(dbData).Except(dbData, new StagingBatchSummaryComparer());
                        if (modifiedData.Any())
                        {
                            sql =
                                @"update StagingBatchSummary set [Name] = @Name, [Amount] = @Amount, [Count] = @Count, [IsClosed] = @IsClosed, 
                                    [Type] = @Type, [PaymentMethod] = @PaymentMethod, [BatchUrl] = @BatchUrl, [Retrieved] = @Retrieved
                                where Id = @Id";
                            await con.ExecuteAsync(sql, modifiedData, tx);
                        }
                        tx.Commit();
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        ex.Data.Add("BatchSummarySql", sql);
                        throw ex;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve batch summaries. Will utilize local staging table first, invoking Concur API if no batches exist.
        /// </summary>
        /// <returns></returns>
        public async Task<List<StagingBatchSummary>> GetStagingBatchSummariesAsync()
        {
            using (var con = new SqlConnection(_connection))
            {
                await con.OpenAsync();
                var sql = "select * from StagingBatchSummaries";
                var data = await con.QueryAsync<StagingBatchSummary>(sql);
                if (data == null || !data.Any())
                {
                    await RefreshStagingBatchSummaryAsync();
                    data = await con.QueryAsync<StagingBatchSummary>(sql);
                }
                var results = data.ToList();
                foreach (var r in results)
                {
                    r.IsFileLocal = File.Exists(Path.Combine(_settings.BatchFolderPath, r.FileName));
                }
                return results;
            }
        }

        /// <summary>
        /// Retrieve details from local database, including staged entries (if any)
        /// </summary>
        /// <param name="id">Concur batch id</param>
        public async Task<StagingBatchSummary> GetStagingBatchSummaryAsync(string id)
        {
            using (var con = new SqlConnection(_connection))
            {
                await con.OpenAsync();
                var sql = @"select * from StagingBatchSummaries where Id = @id;
                    select * from StagingJournalEntry where ConcurBatchID = @id order by batch_skey, seq_nbr, line_nbr";
                using (var multi = await con.QueryMultipleAsync(sql, new { id }))
                {
                    var data = await multi.ReadSingleAsync<StagingBatchSummary>();
                    data.StagedEntries = (await multi.ReadAsync<StagingJournalEntry>()).ToList();
                    data.IsFileLocal = File.Exists(Path.Combine(_settings.BatchFolderPath, data.FileName));
                    return data;
                }
            }
        }

        /// <summary>
        /// Gets row count of je_interface table in BST.
        /// </summary>
        /// <returns>Row count of the BST je_interface table (integer)</returns>
        public int GetInterfaceTableRowCount()
        {
            int rowCount = 1; 

            using (var con = new SqlConnection(_connection))
            {
                
                var sql = @"select count(*) from Proddb..je_interface";

                using (SqlCommand cmdCount = new SqlCommand(sql, con))
                {
                    con.Open();
                    rowCount = (int)cmdCount.ExecuteScalar();
                    con.Close();
                }
            }

            return rowCount;
        }

        /// <summary>
        /// Attempts to close batch in Concur and retrieve the resulting CSV file.
        /// </summary>
        /// <param name="batch">Batch. Must have ID, Name, and BatchUrl set.</param>
        /// <returns>Local file path to CSV file</returns>
        public string CloseAndSaveBatchFile(StagingBatchSummary batch)
        {
            if (batch == null)
            {
                throw new ArgumentNullException(nameof(batch));
            }
            _log.Debug("Attempting to close batch id {BatchID} - {BatchName}", batch.Id, batch.Name);
            var batchFileUrl = CloseBatch(batch.BatchUrl);
            if (batchFileUrl.IsEmpty())
            {
            }
            if (batchFileUrl == batch.BatchUrl)
            {
                _log.Debug("Batch urls match after re-closing {BatchID} ==>> {BatchUrl}", batch.Id, batch.BatchUrl);
            }
            else
            {
                _log.Debug("New batch url {BatchUrl} after re-closing {@Batch}", batchFileUrl, batch);
                batch.BatchUrl = batchFileUrl;
            }
            return SaveBatchFile(batch);

        }

        /// <summary>
        /// Close batch, preventing any new expenses from entering it. After the batch closes, Concur creates the batch file containing 
        /// the expense transaction information. If a batch ID for an already closed batch is sent, Concur regenerates the batch file 
        /// for the specified batch.
        /// </summary>
        /// <param name="batchUrl">Concur URL to batch</param>
        /// <returns>Concur Url to CSV batch file. Null if error or no data.</returns>
        public string CloseBatch(string batchUrl)
        {
            if (batchUrl.IsEmpty())
            {
                throw new Exception("Batch url required");
            }
            var batch = new BatchSummary { BatchUrl = batchUrl };
            _log.Debug("Closing batch in Concur API");
            _client.Authenticator = _tokenManager.GetAuthenticator(_client);
            var request = new RestRequest($"{batch.BatchUrlPathAndQuery}/close", Method.POST);
            // API documentation as different version (1.1 vs. 1.2), method (PUT vs. POST), and different route (close vs. closed)
            //var request = new RestRequest($"/api/expense/paymentbatch/v1.2/batch/{batch.EncryptedId}/closed", Method.POST);
            request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
            var response = _client.Execute<ConcurCloseBatchResponse>(request);
            if (ConcurApiHelper.HasRestError(_log, response))
            {
                return null;
            }
            if (response.Data == null)
            {
                _log.Error($"No data returned {response.Content}");
                return null;
            }
            _log.Information("Batch closed {@Data}", response.Data);
            return response.Data.FileUrl;
        }

        /// <summary>
        /// Save the expense transaction data for the specified payment batch.
        /// API note: If you recently approved reports for payment, wait 30 minutes and try again. Otherwise it'll throw a 500 error.
        /// </summary>
        /// <param name="batch">Batch. Must have ID, Name, and BatchUrl set.</param>
        /// <returns>Local file path to batch file</returns>
        public string SaveBatchFile(StagingBatchSummary batch)
        {
            if (batch == null)
            {
                throw new ArgumentNullException(nameof(batch));
            }
            if (string.IsNullOrWhiteSpace(_settings.BatchFolderPath) || !Directory.Exists(_settings.BatchFolderPath))
            {
                throw new MissingBatchDirectoryException(_settings.BatchFolderPath);
            }
            string filePath = Path.Combine(_settings.BatchFolderPath, batch.FileName);
            _log.Debug("Retrieving batch file from Concur API");
            _client.Authenticator = _tokenManager.GetAuthenticator(_client);
            var requestRoute = batch.BatchUrlPathAndQuery.EndsWith("/file") ? batch.BatchUrlPathAndQuery : $"{batch.BatchUrlPathAndQuery}/file";
            var request = new RestRequest(requestRoute, Method.GET);
            //var request = new RestRequest($"/api/expense/paymentbatch/v1.1/batch/{encryptedId}/file", Method.GET);
            request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
            var response = _client.Execute(request);
            if (ConcurApiHelper.HasRestError(_log, response))
            {
                return null;
            }
            if (response.RawBytes == null || response.RawBytes.Length == 0)
            {
                _log.Error($"No bytes returned {response.Content}");
                return null;
            }
            File.WriteAllBytes(filePath, response.RawBytes);
            _log.Information("Batch file saved {FilePath}", filePath);
            return filePath;
        }

        /// <summary>
        /// Stage given batch from CSV file to database table. Will close batch in Concur and/or retrieve file, if needed.
        /// If already staged, it will delete those files as part of the DB transaction.
        /// </summary>
        /// <returns>Number of staged journal entries saved</returns>
        public async Task<int> StageAsync(StagingBatchSummary batch)
        {
            _log.Debug("Staging batch {Id}", batch.Id);
            string filePath = Path.Combine(_settings.BatchFolderPath, batch.FileName);
            if (!batch.IsClosed)
            {
                CloseAndSaveBatchFile(batch);
            }
            else if (!File.Exists(filePath))
            {
                SaveBatchFile(batch);
            }
            _log.Debug("Retrieving source data from batch CSV file {filePath}", filePath);
            var concurBatches = ProcessBatchFile(filePath);
            var employees = GetBstEmployees();
            var periods = GetBstPeriodEnds();
            List<StagingJournalEntry> journalEntries = ConvertBatchToStaging(batch.Id, concurBatches, employees, periods);
            if (!journalEntries.Any())
            {
                return 0;
            }
            string sql = "";
            using (var con = new SqlConnection(_connection))
            {
                await con.OpenAsync();
                using (var tx = con.BeginTransaction(System.Data.IsolationLevel.ReadCommitted, "SaveStagingJournalEntries"))
                {
                    try
                    {
                        sql = "delete StagingJournalEntry where ConcurBatchID = @ConcurBatchID";
                        await con.ExecuteAsync(sql, new { ConcurBatchID = batch.Id }, tx);

                        sql =
                            @"insert StagingJournalEntry
	                        ([batch_skey], 
	                        [co_code], 
	                        [usr_batch_id], 
	                        [per_end_date], 
	                        [sys_doc_type_code], 
	                        [usr_doc_type_suf], 
	                        [max_dcml_places], 
	                        [input_date], 
	                        [batch_doc_total], 
	                        [batch_acct_db_total], 
	                        [batch_acct_cr_total], 
	                        [batch_co_db_total], 
	                        [batch_co_cr_total], 
	                        [seq_nbr], 
	                        [doc_nbr], 
	                        [doc_date], 
	                        [unit_input_yn], 
	                        [currency_code], 
	                        [currency_rate], 
	                        [reversal_yn], 
	                        [reversal_per_end_date], 
	                        [doc_acct_db_total], 
	                        [doc_acct_cr_total], 
	                        [doc_co_db_total], 
	                        [doc_co_cr_total], 
	                        [line_nbr], 
	                        [trn_co_code], 
	                        [org_code], 
	                        [acct_code], 
	                        [evc_type_code], 
	                        [evc_code], 
	                        [extern_ref_nbr], 
	                        [extern_ref_date], 
	                        [intern_ref_nbr], 
	                        [trn_date], 
	                        [wbs_ind], 
	                        [db_amt_ac], 
	                        [cr_amt_ac], 
	                        [db_amt_cc], 
	                        [cr_amt_cc], 
	                        [prj_code], 
	                        [phase_code], 
	                        [task_code], 
	                        [inv_grouping_code], 
	                        [actv_code], 
	                        [rsrc_code], 
	                        [qty_amt], 
	                        [pcs_desc], 
	                        [fcs_desc],
                            [ConcurBatchID],
                            [StageId],
                            [Staged])
                        values
	                        (@batch_skey, 
	                        @co_code, 
	                        @usr_batch_id, 
	                        @per_end_date, 
	                        @sys_doc_type_code, 
	                        @usr_doc_type_suf, 
	                        @max_dcml_places, 
	                        @input_date, 
	                        @batch_doc_total, 
	                        @batch_acct_db_total, 
	                        @batch_acct_cr_total, 
	                        @batch_co_db_total, 
	                        @batch_co_cr_total, 
	                        @seq_nbr, 
	                        @doc_nbr, 
	                        @doc_date, 
	                        @unit_input_yn, 
	                        @currency_code, 
	                        @currency_rate, 
	                        @reversal_yn, 
	                        @reversal_per_end_date, 
	                        @doc_acct_db_total, 
	                        @doc_acct_cr_total, 
	                        @doc_co_db_total, 
	                        @doc_co_cr_total, 
	                        @line_nbr, 
	                        @trn_co_code, 
	                        @org_code, 
	                        @acct_code, 
	                        @evc_type_code, 
	                        @evc_code, 
	                        @extern_ref_nbr, 
	                        @extern_ref_date, 
	                        @intern_ref_nbr, 
	                        @trn_date, 
	                        @wbs_ind, 
	                        @db_amt_ac, 
	                        @cr_amt_ac, 
	                        @db_amt_cc, 
	                        @cr_amt_cc, 
	                        @prj_code, 
	                        @phase_code, 
	                        @task_code, 
	                        @inv_grouping_code, 
	                        @actv_code, 
	                        @rsrc_code, 
	                        @qty_amt, 
	                        @pcs_desc, 
	                        @fcs_desc,
                            @ConcurBatchID,
                            @StageId,
                            sysdatetimeoffset());";
                        await con.ExecuteAsync(sql, journalEntries, tx);

                        tx.Commit();
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        _log.Error(ex, "SQL error for batch {Id}: {Message}. SQL: {Sql} Entries: {@JournalEntries}", batch.Id, ex.Message, sql, journalEntries);
                        return 0;
                    }
                }
            }
            return journalEntries.Count;
        }

        public static List<ConcurBatch> ProcessBatchFile(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath))
            {
                throw new ArgumentException("Input file path is required", nameof(filePath));
            }
            if (!File.Exists(filePath))
            {
                throw new ArgumentException("Input file does not exist", nameof(filePath));
            }
            using (var reader = File.OpenText(filePath))
            {
                using (var csv = new CsvReader(reader))
                {
                    csv.Configuration.PrepareHeaderForMatch = header => header.Replace(" ", string.Empty).Trim();
                    //csv.Configuration.RegisterClassMap<ConcurBatchMap>();
                    return csv.GetRecords<ConcurBatch>().ToList();
                }
            }
        }

        public async Task UndoBatchToBst(string concurBatchId)
        {
            using (var con = new SqlConnection(_connection))
            {
                await con.OpenAsync();
                using (var tx = con.BeginTransaction(System.Data.IsolationLevel.ReadCommitted, "UndoBatchToBst"))
                {
                    var sql = "select * from StagingJournalEntry where ConcurBatchID = @ConcurBatchID order by batch_skey, seq_nbr, line_nbr";
                    try
                    {
                        List<StagingJournalEntry> stagedEntries = (await con.QueryAsync<StagingJournalEntry>(sql, new { ConcurBatchID = concurBatchId }, tx)).ToList();
                        if (stagedEntries.Any(x => x.Completed.HasValue))
                        {
                            sql = "update StagingJournalEntry set Completed = null where ConcurBatchID = @ConcurBatchID";
                            await con.ExecuteAsync(sql, new { ConcurBatchID = concurBatchId }, tx);
                        }
                        sql = "delete ProdDb..je_interface where batch_skey = @batch_skey and seq_nbr = @seq_nbr and line_nbr = @line_nbr";
                        await con.ExecuteAsync(sql, stagedEntries, tx);
                        tx.Commit();
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        ex.Data.Add("UndoBatchToBstSql", sql);
                        throw ex;
                    }
                }
            }
        }

        public async Task SaveBatchToBst(string concurBatchId)
        {
            using (var con = new SqlConnection(_connection))
            {
                await con.OpenAsync();
                using (var tx = con.BeginTransaction(System.Data.IsolationLevel.ReadCommitted, "SaveBatchToBst"))
                {
                    var sql = "select * from StagingJournalEntry where ConcurBatchID = @ConcurBatchID order by batch_skey, seq_nbr, line_nbr";
                    try
                    {
                        //Check that destination table in BST (je_interface) is empty before saving batch. If not empty, throw exception.
                        
                        if (GetInterfaceTableRowCount() > 0)
                        {
                            throw new BSTInterfaceTableFull(concurBatchId);
                        }                        

                        List<BstJournalEntryInterface> journalEntries = (await con.QueryAsync<BstJournalEntryInterface>(sql, new { ConcurBatchID = concurBatchId }, tx)).ToList();
                        if (!journalEntries.Any())
                        {
                            throw new MissingStagedJournalEntriesException(concurBatchId);
                        }
                        sql = "update StagingJournalEntry set Completed = sysdatetimeoffset() where ConcurBatchID = @ConcurBatchID";
                        await con.ExecuteAsync(sql, new { ConcurBatchId = concurBatchId }, tx);

                        sql =
                            @"insert ProdDb..je_interface
	                            ([batch_skey], 
	                            [co_code], 
	                            [usr_batch_id], 
	                            [per_end_date], 
	                            [sys_doc_type_code], 
	                            [usr_doc_type_suf], 
	                            [max_dcml_places], 
	                            [input_date], 
	                            [batch_doc_total], 
	                            [batch_acct_db_total], 
	                            [batch_acct_cr_total], 
	                            [batch_co_db_total], 
	                            [batch_co_cr_total], 
	                            [seq_nbr], 
	                            [doc_nbr], 
	                            [doc_date], 
	                            [unit_input_yn], 
	                            [currency_code], 
	                            [currency_rate], 
	                            [reversal_yn], 
	                            [reversal_per_end_date], 
	                            [doc_acct_db_total], 
	                            [doc_acct_cr_total], 
	                            [doc_co_db_total], 
	                            [doc_co_cr_total], 
	                            [line_nbr], 
	                            [trn_co_code], 
	                            [org_code], 
	                            [acct_code], 
	                            [evc_type_code], 
	                            [evc_code], 
	                            [extern_ref_nbr], 
	                            [extern_ref_date], 
	                            [intern_ref_nbr], 
	                            [trn_date], 
	                            [wbs_ind], 
	                            [db_amt_ac], 
	                            [cr_amt_ac], 
	                            [db_amt_cc], 
	                            [cr_amt_cc], 
	                            [prj_code], 
	                            [phase_code], 
	                            [task_code], 
	                            [inv_grouping_code], 
	                            [actv_code], 
	                            [rsrc_code], 
	                            [qty_amt], 
	                            [pcs_desc], 
	                            [fcs_desc])
                            values
	                            (@batch_skey, 
	                            @co_code, 
	                            @usr_batch_id, 
	                            @per_end_date, 
	                            @sys_doc_type_code, 
	                            @usr_doc_type_suf, 
	                            @max_dcml_places, 
	                            @input_date, 
	                            @batch_doc_total, 
	                            @batch_acct_db_total, 
	                            @batch_acct_cr_total, 
	                            @batch_co_db_total, 
	                            @batch_co_cr_total, 
	                            @seq_nbr, 
	                            @doc_nbr, 
	                            @doc_date, 
	                            @unit_input_yn, 
	                            @currency_code, 
	                            @currency_rate, 
	                            @reversal_yn, 
	                            @reversal_per_end_date, 
	                            @doc_acct_db_total, 
	                            @doc_acct_cr_total, 
	                            @doc_co_db_total, 
	                            @doc_co_cr_total, 
	                            @line_nbr, 
	                            @trn_co_code, 
	                            @org_code, 
	                            @acct_code, 
	                            @evc_type_code, 
	                            @evc_code, 
	                            @extern_ref_nbr, 
	                            @extern_ref_date, 
	                            @intern_ref_nbr, 
	                            @trn_date, 
	                            @wbs_ind, 
	                            @db_amt_ac, 
	                            @cr_amt_ac, 
	                            @db_amt_cc, 
	                            @cr_amt_cc, 
	                            @prj_code, 
	                            @phase_code, 
	                            @task_code, 
	                            @inv_grouping_code, 
	                            @actv_code, 
	                            @rsrc_code, 
	                            @qty_amt, 
	                            @pcs_desc, 
	                            @fcs_desc);";
                        await con.ExecuteAsync(sql, journalEntries, tx);
                        tx.Commit();
                    }
                    catch (Exception ex)
                    {
                        tx.Rollback();
                        ex.Data.Add("SaveBatchToBstSql", sql);
                        throw ex;
                    }
                }
            }
        }

        public List<StagingJournalEntry> ConvertBatchToStaging(string concurBatchId, List<ConcurBatch> batch, List<BstEmployee> employees, List<DateTime> periods)
        {
            _log.Debug("Converting batch file to staging table of BST-compatible journal entries");
            var journalEntries = new List<StagingJournalEntry>();

            int seqCounter = 0;
            var reports = from report in batch group report by report.ReportID into g select new { ReportID = g.Key, Lines = g };
            foreach (var r in reports)
            {
                seqCounter++;
                int lineCounter = 0;
                foreach (var line in r.Lines)
                {
                    var je = line.ToJournalEntry(employees, periods);
                    je.ConcurBatchID = concurBatchId;
                    je.line_nbr = ++lineCounter;
                    je.seq_nbr = seqCounter;
                    je.doc_nbr = "Concur"; //seqCounter.ToString();
                    je.batch_doc_total = 1;
                    journalEntries.Add(je);

                    // Need to add second entry depending on batch type
                    // - If company paid (CBCP), negate amount, Account=8001, project structure = null
                    // - If employee paid (CBEP), negate amount, Account=1018, batches grouped by emp id
                    je = line.ToJournalEntryPayment(employees, periods);
                    je.ConcurBatchID = concurBatchId;
                    je.line_nbr = ++lineCounter;
                    je.seq_nbr = seqCounter;
                    //je.doc_nbr = seqCounter.ToString(); //per Accounting, this should always be "Concur"
                    je.doc_nbr = "Concur";
                    je.batch_doc_total = 1;  //per Accounting, this field should be populated for second entries as well
                    journalEntries.Add(je);                    
                }
            }

            // Given that we need to add counter entries above, recalc totals and subtotals once all entries are squared away
            var creditTotal = journalEntries.Sum(x => x.cr_amt_ac);
            var debitTotal = journalEntries.Sum(x => x.db_amt_ac);
            var docs = journalEntries.GroupBy(x => x.seq_nbr)
                .Select(x => new { Seq = x.Key, Credits = x.Sum(s => s.cr_amt_ac), Debits = x.Sum(s => s.db_amt_ac) } )
                .ToList();
            foreach (var je in journalEntries)
            {
                je.batch_acct_cr_total = creditTotal;
                je.batch_acct_db_total = debitTotal;
                je.batch_co_cr_total = creditTotal;
                je.batch_co_db_total = debitTotal;

                var doc = docs.First(x => x.Seq == je.seq_nbr);
                je.doc_acct_cr_total = doc.Credits;
                je.doc_acct_db_total = doc.Debits;
                je.doc_co_cr_total = doc.Credits;
                je.doc_co_db_total = doc.Debits;
            }

            return journalEntries;
        }

        private List<DateTime> GetBstPeriodEnds()
        {
            _log.Debug("Retrieving period ends from BST database");
            var sql =
                @"select per_end_date
                from Proddb..period_end_dates
                where rec_status_ind = 'A'
                    and labor_period_yn = 'N'
                    and expense_period_yn = 'N'
                    and fiscal_year >= year(sysdatetime()) - 1";
            using (var con = new SqlConnection(_connection))
            {
                con.Open();
                return con.Query<DateTime>(sql).ToList();
            }
        }

        private List<BstEmployee> GetBstEmployees()
        {
            _log.Debug("Retrieving employees from BST database");
            var sql = 
                @"select
	                Code = e.emp_code, 
	                [Name] = e.emp_name, 
	                [Status] = e.rec_status_ind,
	                OrgCode = e.org_code
                from Proddb..emp_info e with (nolock)";
            using (var con = new SqlConnection(_connection))
            {
                con.Open();
                return con.Query<BstEmployee>(sql).ToList();
            }
        }
    }
}