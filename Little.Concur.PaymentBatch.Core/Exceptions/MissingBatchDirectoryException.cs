﻿using System;

namespace Little.Concur.PaymentBatch.Core.Exceptions
{
    public class MissingBatchDirectoryException : Exception
    {
        public string Path { get; set; }

        public MissingBatchDirectoryException(string path)
            : base($"Batch file target directory missing: {path}")
        {
            Path = path;
        }

        public MissingBatchDirectoryException(string path, string sMessage)
            : base(sMessage)
        {
            Path = path;
        }

        public MissingBatchDirectoryException(string path, string sMessage, params Object[] args)
            : base(string.Format(sMessage, args))
        {
            Path = path;
        }
    }
}