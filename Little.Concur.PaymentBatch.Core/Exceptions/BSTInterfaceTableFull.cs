﻿using System;

namespace Little.Concur.PaymentBatch.Core.Exceptions
{
    public class BSTInterfaceTableFull : Exception       
    {
        public string ConcurBatchId { get; set; }

        public BSTInterfaceTableFull(string concurBatchId)
            : base($"Error - the BST je_interface table must be empty before proceeding. Concur batch ID {concurBatchId}.")
        {
            ConcurBatchId = concurBatchId;
        }
    }
}
