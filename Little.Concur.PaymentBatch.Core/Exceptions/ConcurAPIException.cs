﻿using System;

namespace Little.Concur.PaymentBatch.Core.Exceptions
{
    public class ConcurAPIException : Exception
    {
        public string Path { get; set; }

        public ConcurAPIException()
            : base("Unknown error with Concur API")
        {}

        public ConcurAPIException(string sMessage)
            : base(sMessage)
        {}

        public ConcurAPIException(string sMessage, params Object[] args)
            : base(string.Format(sMessage, args))
        {}
    }
}