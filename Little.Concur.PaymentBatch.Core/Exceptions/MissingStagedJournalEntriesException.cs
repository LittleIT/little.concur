﻿using System;

namespace Little.Concur.PaymentBatch.Core.Exceptions
{
    public class MissingStagedJournalEntriesException : Exception
    {
        public string ConcurBatchId { get; set; }

        public MissingStagedJournalEntriesException(string concurBatchId)
            : base($"Unable to migrate to BST interface table: No staged journal entries for Concur Batch ID {concurBatchId}")
        {
            ConcurBatchId = concurBatchId;
        }

        public MissingStagedJournalEntriesException(string concurBatchId, string sMessage)
            : base(sMessage)
        {
            ConcurBatchId = concurBatchId;
        }

        public MissingStagedJournalEntriesException(string concurBatchId, string sMessage, params Object[] args)
            : base(string.Format(sMessage, args))
        {
            ConcurBatchId = concurBatchId;
        }
    }
}