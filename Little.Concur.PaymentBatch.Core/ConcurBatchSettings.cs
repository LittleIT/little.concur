﻿namespace Little.Concur.PaymentBatch.Core
{
    public class ConcurBatchSettings : ConcurSettings
    {
        public string BatchFolderPath { get; set; }
    }
}