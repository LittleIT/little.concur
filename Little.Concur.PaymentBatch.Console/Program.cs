﻿using System;
using System.IO;
using Little.Concur.PaymentBatch.Core;
using Microsoft.Extensions.CommandLineUtils;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;

namespace Little.Concur.PaymentBatch
{
    class Program
    {
        public const string AppName = "LittleConcurPaymentBatch";

        static void Main(string[] args)
        {
            Console.Title = AppName;
            try
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json");
                var config = builder.Build();
                var settings = new ConcurBatchSettings();
                config.GetSection("ConcurSettings").Bind(settings);
                string dataConnection = config.GetConnectionString($"{settings.Environment}Connection");
                string loggingConnection = config.GetConnectionString("LoggingConnection");
                Log.Logger = new LoggerConfiguration()
                    .MinimumLevel.Debug()
                    .Enrich.WithMachineName()
                    .Enrich.WithEnvironmentUserName()
                    .Enrich.WithProcessName()
                    .WriteTo.Debug()
                    .WriteTo.Console(restrictedToMinimumLevel: LogEventLevel.Information)
                    .WriteTo.MSSqlServer(loggingConnection, "Logs", LogEventLevel.Information)
                    .CreateLogger();

                var app = new CommandLineApplication();
                app.Name = AppName;
                app.HelpOption("-?|-h|--help");
                app.OnExecute(() =>
                {
                    Console.WriteLine("Console deprecated. Use web site.");
                    //try
                    //{
                    //    var mgr = new ConcurPaymentBatchIntegrator(settings, dataConnection);
                    //    mgr.Execute();
                    //}
                    //catch (Exception ex)
                    //{
                    //    var log = Log.ForContext<ConcurPaymentBatchIntegrator>();
                    //    log.Error(ex, ex.Message);
                    //}
                    //finally
                    //{
                    //    Log.CloseAndFlush();
                    //}
                    return 0;
                });
                app.Execute(args);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Fatal error: unable to configure or setup application.");
                Console.WriteLine(ex.Message);
            }
        }
    }
}