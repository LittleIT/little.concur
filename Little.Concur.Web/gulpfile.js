﻿/// <binding BeforeBuild='min' Clean='clean' />
"use strict";
var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify");

var paths = {
    webroot: "./Content/"
};
paths.js = paths.webroot + "app/js/**/*.js";
paths.minJs = paths.webroot + "app/js/**/*.min.js";
paths.css = paths.webroot + "app/css/**/*.css";
paths.minCss = paths.webroot + "app/css/**/*.min.css";
paths.debugJsDest = paths.webroot + "js/site.debug.js";
paths.concatJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "css/site.min.css";
paths.vendorSource = "./node_modules/";
paths.vendorTarget = paths.webroot + "vendor/";

gulp.task("clean:js", function (cb) {
    rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    rimraf(paths.concatCssDest, cb);
});

gulp.task("clean", ["clean:js", "clean:css"]);

gulp.task("debug:js", function () {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.debugJsDest))
        .pipe(gulp.dest("."));
});

gulp.task("min:js", function () {
    return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});

gulp.task("min:css", function () {
    return gulp.src([paths.css, "!" + paths.minCss])
        .pipe(concat(paths.concatCssDest))
        .pipe(cssmin())
        .pipe(gulp.dest("."));
});

gulp.task("min", ["debug:js", "min:js", "min:css"]);

gulp.task("vendor:jquery", function () {
    return gulp.src(paths.vendorSource + 'jquery/dist/*')
        .pipe(gulp.dest(paths.vendorTarget + "jquery/"));
});

gulp.task("vendor:bootstrap", function () {
    return gulp.src(paths.vendorSource + 'bootstrap/dist/**')
        .pipe(gulp.dest(paths.vendorTarget + "bootstrap/"));
});


gulp.task("vendor:font-awesome-fonts", function () {
    return gulp.src(paths.vendorSource + 'font-awesome/fonts/*')
        .pipe(gulp.dest(paths.vendorTarget + "font-awesome/fonts/"));
});

gulp.task("vendor:font-awesome-css", function () {
    return gulp.src(paths.vendorSource + 'font-awesome/css/*')
        .pipe(gulp.dest(paths.vendorTarget + "font-awesome/css/"));
});

gulp.task("vendor:datatables", function () {
    return gulp.src(paths.vendorSource + 'datatables*/**')
        .pipe(gulp.dest(paths.vendorTarget + "datatables/"));
});

//gulp.task("vendor:chartjs", function () {
//    return gulp.src(paths.vendorSource + 'chart.js/dist/*.js')
//        .pipe(gulp.dest(paths.vendorTarget + "chartjs/"));
//});

gulp.task("vendor:jsrender", function () {
    return gulp.src(paths.vendorSource + 'jsrender/*')
        .pipe(gulp.dest(paths.vendorTarget + "jsrender/"));
});

gulp.task("vendor:jquery-serializejson", function () {
    return gulp.src(paths.vendorSource + 'jquery-serializejson/*')
        .pipe(gulp.dest(paths.vendorTarget + "jquery-serializejson/"));
});

gulp.task("vendor:popper", function () {
    return gulp.src(paths.vendorSource + 'popper.js/dist/umd/*')
        .pipe(gulp.dest(paths.vendorTarget + "popper/"));
});

gulp.task("vendor", [
    "vendor:jquery",
    "vendor:popper",
    "vendor:bootstrap",
    "vendor:font-awesome-fonts",
    "vendor:font-awesome-css",
    "vendor:datatables",
    //"vendor:chartjs",
    "vendor:jsrender",
    "vendor:jquery-serializejson"
]);