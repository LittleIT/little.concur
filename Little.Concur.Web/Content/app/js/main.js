﻿$(function () {
    $("form.ajax").submit(Utilities.AjaxSubmit);
    $("#view-next").click(function () {
        $("#ExpenseReportsForm").submit();
    });
    $("#EmpEmail").change(function () {
        $("#NextPageUrl").val("");
    });
    $.views.settings.debugMode(true);
    $.views.converters("date", function (val) {
        if (val == null) { return ""; }
        return val.length > 10 ? val.substr(0, 10) : val;
    });
    $.views.converters("decimal", function (val) {
        if (val == null) { return ""; }
        return Utilities.FormatNumber(val, 2);
    });
    $.templates({
        viewCount: "<span>Displaying {{:Items.length}} expense reports</span>",
        viewExpenseReports:
        "{{for Items}}\
                <tr>\
                    <td>{{:OwnerName}}</td>\
                    <td>{{:Name}}</td>\
                    <td class=\"text-right\">{{decimal:Total}}</td>\
                    <td>{{if SubmitDate && SubmitDate != '0001-01-01T00:00:00'}}{{date:SubmitDate}}{{/if}}</td>\
                    <td>{{:ApproverName}}</td>\
                    <td>{{:ApprovalStatusName}}</td>\
                    <td>{{if PaidDate && PaidDate != '0001-01-01T00:00:00'}}{{date:PaidDate}}{{/if}}</td>\
                    <td>{{if HasException}}Exception{{/if}}</td>\
                </tr>\
                {{/for}}"
    });
    $("#ExpenseReportsForm").submit(function (e) {
        e.preventDefault();
        var $form = $(this);
        var $submitButton = $form.find(".submitButton");
        var $msg = Utilities.GetMessageContainer(this, ".alert-success", "#pageMessage");
        var $error = Utilities.GetMessageContainer(this, ".alert-danger", "#pageError");
        var url = $form.attr("action") + $("#EmpEmail").val() + ($("#NextPageUrl").val() ? "?" + $form.serialize() : "");
        $submitButton.button('loading');
        $.ajax({
            method: 'GET',
            dataType: "json",
            url: url
        }).done(function (result) {
            var msgHtml = $.templates.viewCount(result);
            $msg.html(msgHtml).fadeIn();
            var footer = $("#ExpenseReports tfoot th");
            $("#NextPageUrl").val(result.NextPage);
            if (result.NextPage) {
                $("#view-next").prop("disabled", false);
            } else {
                $("#view-next").prop("disabled", true);
            }
            var html = $.templates.viewExpenseReports(result);
            $("#ExpenseReports tbody").html(html);
        }).fail(function () {
            Utilities.Log(result);
            $error.html("<span>Unable to load expense reports.</span>").fadeIn();
        }).always(function () { $submitButton.button('reset'); });
    });
});