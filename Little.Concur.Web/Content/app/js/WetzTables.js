﻿var WetzTables = WetzTables ? WetzTables : function () {
    return {
        init: function () {
            $("table.datatable").each(function () {
                var $table = $(this);
                var colDefs = [];
                var columns = [];
                $(this).find("thead th").each(function (i) {
                    var fieldData = $(this).data("data");
                    var fieldName = $(this).data("name")
                    if (!fieldName) {
                        fieldName = fieldData
                    }
                    columns.push({ "data": fieldData, "name": fieldName, "searchable": $(this).data("searchable") !== false });
                    var renderHelper = $(this).data("renderhelper");
                    switch (renderHelper) {
                        case "renderLink":
                            colDefs.push({ "targets": i, "render": WetzTables.renderLink });
                            break;
                        case "renderLinkView":
                            colDefs.push({ "targets": i, "render": WetzTables.renderLinkView });
                            break;
                        case "renderTrimmer":
                            colDefs.push({ "targets": i, "render": WetzTables.renderTrimmer });
                            break;
                        case "renderDateYear":
                            colDefs.push({ "targets": i, "render": WetzTables.renderDateYear });
                            break;
                        case "renderDecimalTens":
                            colDefs.push({ "targets": i, "render": WetzTables.renderDecimalTens });
                            break;
                        case "renderDecimal":
                            colDefs.push({ "targets": i, "render": WetzTables.renderDecimal });
                            break;
                        case "renderDecimalThousands":
                            colDefs.push({ "targets": i, "render": WetzTables.renderDecimalThousands });
                            break;
                    }
                });
                if (!$table.hasClass("serverside")) {
                    if (colDefs.length === 0) {
                        $table.dataTable();
                    }
                    else {
                        $table.dataTable({ "processing": true, "columnDefs": colDefs });
                    }
                } else {
                    $table.dataTable({
                        "processing": true,
                        "serverSide": true,
                        "columnDefs": colDefs,
                        "columns": columns
                    });
                    var table = $table.DataTable();
                    table.columns().every(WetzTables.DataTableColumnSearch);
                }
            });
        },

        renderLink: function (data, type, row) {
            if (row.url == null) { return data; }
            return '<a href="' + row.url + '">' + data + '</a>';
        },

        renderLinkView: function (data, type, row) {
            if (data == null) { return ""; }
            return '<a href="' + data + '">View</a>';
        },

        renderTrimmer: function (data, type, row) {
            if (data == null) { return ""; }
            return data.length > 50 ? data.substr(0, 48) + '...' : data;
        },

        renderDateYear: function (data, type, row) {
            if (data == null) { return ""; }
            return data.length > 10 ? data.substr(0, 10) : data;
        },

        renderDecimalTens: function (data, type, row) {
            if (data == null) { return ""; }
            return Utilities.FormatNumber(data, 1);
        },

        renderDecimal: function (data, type, row) {
            if (data == null) { return ""; }
            return Utilities.FormatNumber(data, 2);
        },

        renderDecimalThousands: function (data, type, row) {
            if (data == null) { return ""; }
            return Utilities.FormatNumber(data, 3);
        },

        DataTableColumnSearch: function () {
            var c = this;
            $("input", this.footer()).on("keyup change", function () {
                if (c.search() !== this.value) {
                    c.search(this.value).draw();
                }
            });
        }
    }
}();

$(function () {
    WetzTables.init();
});