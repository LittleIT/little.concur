﻿namespace Little.Concur.Web.Models
{
    public class Employee
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string Email { get; set; }
        public string OrgApprover { get; set; }

        public bool IsActive { get { return Status == "A"; } }
    }
}