﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Little.Concur.PaymentBatch.Core.Data;

namespace Little.Concur.Web.Models
{
    public class PaymentBatchesModel
    {
        public string Status { get; set; } = "OPEN";
        public IEnumerable<BatchSummary> Batches { get; set; }
        public IEnumerable<string> Statuses { get { return new string[] { "OPEN", "CLOSED" }; } }
    }
}