﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Dapper;
using Little.Concur.Expenses.Core;
using Little.Concur.PaymentBatch.Core;
using Little.Concur.PaymentBatch.Core.Data;
using Little.Concur.Web.Models;
using Microsoft.Extensions.Configuration;
using Nancy;
using Nancy.Extensions;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Nancy.Validation;
using Serilog;

namespace Little.Concur.Web.Modules
{
    public class PaymentBatchModule : NancyModule
    {
        public const string RouteRoot = "/batch";
        public const string ApiRouteRoot = "/api/batch";

        private readonly IConfiguration _config;
        private readonly IConcurSettings _settings;
        private readonly IConcurTokenManager _tokenManager;
        private readonly ILogger _log;

        public PaymentBatchModule(IConfiguration config, IConcurSettings settings, IConcurTokenManager tokenManager) : base()
        {
            _config = config;
            _settings = settings;
            _tokenManager = tokenManager;
            _log = Log.ForContext<ExpenseReportModule>();
            Get(RouteRoot, async (p, ct) => await ViewLandingAsync(p, ct));
            /*
             * TODO: Create UI for handling batches
                - Display list of eligible batches. Most likely display data from staging tables with option to refresh from Concur. Note if we already have CSV file.
                - Allow user to close batch, optionally forcing generation of CSV (default reuse local copy).
                - Process batch file to staging tables
                - Validate and push to BST interface table
             */
        }

        public async Task<Negotiator> ViewLandingAsync(dynamic p, CancellationToken ct)
        {
            string status = this.Request.Query.Status;
            var mgr = new ConcurPaymentBatchIntegrator((ConcurBatchSettings)_settings, _config.GetConnectionString($"{_settings.Environment}Connection"));
            var model = new PaymentBatchesModel { Status = status };
            model.Batches = await mgr.GetBatchListAsync(status);
            // TODO Utilize model that provides related info, such as whether we already have the CSV batch file local (and perhaps BST-related info, if possible)
            return View["batch-landing.cshtml", model];
        }
    }
}