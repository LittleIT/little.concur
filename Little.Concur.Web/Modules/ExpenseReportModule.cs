﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using Dapper;
using Little.Concur.Expenses.Core;
using Little.Concur.Web.Models;
using Microsoft.Extensions.Configuration;
using Nancy;
using Nancy.Extensions;
using Nancy.ModelBinding;
using Nancy.Responses.Negotiation;
using Nancy.Validation;
using Serilog;

namespace Little.Concur.Web.Modules
{
    public class ExpenseReportModule : NancyModule
    {
        public const string RouteRoot = "/expense-reports";
        public const string ApiRouteRoot = "/api/expense-reports";

        private readonly IConfiguration _config;
        private readonly IConcurSettings _settings;
        private readonly IConcurTokenManager _tokenManager;
        private readonly ILogger _log;

        public ExpenseReportModule(IConfiguration config, IConcurSettings settings, IConcurTokenManager tokenManager)
            : base()
        {
            _config = config;
            _settings = settings;
            _tokenManager = tokenManager;
            _log = Log.ForContext<ExpenseReportModule>();

            Get(RouteRoot, p => ViewLanding(p));
            Get(ApiRouteRoot + $"{RouteRoot}/{{user}}", p => ViewByUser(p));
            Get(ApiRouteRoot + $"{RouteRoot}/{{user}}/{{report}}", p =>
            {
                return Negotiate.WithModel(new { Message = "NYI" });
            });
        }

        //public async Task<Negotiator> ViewLanding(dynamic p, CancellationToken ct)
        private Negotiator ViewLanding(dynamic p)
        {
            this.Context.ViewBag.Title = "Expense Reports";
            /*
            _log.Debug("Retrieving employees from BST database");
            var sql =
                @"select
	            Code = e.emp_code, 
	            [Name] = e.emp_name, 
	            [Status] = e.rec_status_ind,
	            Email = c.email_addr,
				OrgApprover = o.emp_code
            from dbo.emp_info e with (nolock)
	            inner join dbo.emp_correspond c with (nolock) on e.emp_code = c.emp_code
				inner join dbo.org_code o ON e.org_code = o.org_code AND e.co_code = o.co_code
            where
	            e.emp_code <> '******'
	            and e.emp_name like '%,%'
	            and c.email_addr is not null
				and o.org_status_ind = 'A'";
            using (var con = new SqlConnection(_config.GetConnectionString($"{_settings.Environment}Connection")))
            {
                await con.OpenAsync();
                return await con.QueryAsync<Employee>(sql);
            }
            */
            var model = new List<Employee>
            {
                new Employee { Email = "alpha@test.com", Name = "Alpha Test", Code = "0000" },
                new Employee { Email = "terry.bradshaw@littleonline.com", Name = "TERRY BRADSHAW", Code = "1111" },
                new Employee { Email = "lbell@littleonline.com", Name = "Linda Bell", Code = "2222" }
            };
            return View["expense-reports.cshtml", model];
        }

        //public async Task<Negotiator> ViewByUser(dynamic p, CancellationToken ct)
        private Negotiator ViewByUser(dynamic p)
        {
            string userId = p.user ?? "ALL";
            string nextPageUrl = this.Context.Request.Query.NextPageUrl;
            this.Context.ViewBag.Title = $"Expense Reports: {userId}";
            var engine = new ConcurExpenseIntegrator(_settings, _tokenManager);
            //var model = await engine.GetExpenseReportsAsync(userId, nextPageUrl);
            var model = engine.GetExpenseReports(userId, nextPageUrl);
            return Negotiate.WithModel(model);
        }
    }
}