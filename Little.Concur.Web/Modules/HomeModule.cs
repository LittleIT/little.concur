﻿using Nancy;

namespace Little.Concur.Web.Modules
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
            : base()
        {
            Get("/", p =>
            {
                this.Context.ViewBag.Title = "Little/Concur Home";
                return View["home.cshtml"];
            });
        }
    }
}