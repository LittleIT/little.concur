﻿using System;
using Little.Concur.PaymentBatch.Core;
using Microsoft.Extensions.Configuration;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.Configuration;
using Nancy.Cryptography;
using Nancy.Diagnostics;
using Nancy.Extensions;
using Nancy.Security;
using Nancy.TinyIoc;
using Newtonsoft.Json.Linq;
using Serilog;
using Serilog.Events;

namespace Little.Concur.Web
{
    public class WebBootstrapper : DefaultNancyBootstrapper
    {
        public override void Configure(INancyEnvironment environment)
        {
            environment.Diagnostics(
                enabled: true,
                password: "4E40061d-D9CF-48EA-8C0D-35E6F77f4020",
                path: "/_nancy",
                cookieName: "__nancy_diagnostics",
                slidingTimeout: 30,
                cryptographyConfiguration: CryptographyConfiguration.Default);
// Known issue: Razor views in Nancy are cached while debugging. This workaround let's you actually work on them.
#if DEBUG
            environment.Views(runtimeViewUpdates: true);
#endif
        }

        protected override void ConfigureApplicationContainer(TinyIoCContainer container)
        {
            base.ConfigureApplicationContainer(container);
            var builder = new ConfigurationBuilder()
                //.SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            var config = builder.Build();
            var settings = new ConcurBatchSettings();
            config.GetSection("ConcurSettings").Bind(settings);
            container.Register<IConfiguration>(config);
            container.Register<IConcurSettings>(settings);
            container.Register<IConcurTokenManager, ConcurTokenManager>();

            string loggingConnection = config.GetConnectionString("LoggingConnection");
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .Enrich.WithMachineName()
                .Enrich.WithEnvironmentUserName()
                .WriteTo.MSSqlServer(loggingConnection, "Logs", LogEventLevel.Information)
                .CreateLogger();
        }

        protected override void ApplicationStartup(TinyIoCContainer container, IPipelines pipelines)
        {
            base.ApplicationStartup(container, pipelines);
            Csrf.Enable(pipelines);
            pipelines.OnError += HandleError;
        }

        private Response HandleError(NancyContext ctx, Exception ex)
        {
            Log.Error("Error encountered: {@Ex}{NewLine}Full context: {@Ctx}", ex, ctx);
            if (ctx.Request.IsAjaxRequest())
            {
                var response = ctx.Response == null ? new Response() : ctx.Response;
                response.StatusCode = HttpStatusCode.InternalServerError;
                response.ContentType = "application/json";
                var sJson = JObject.FromObject(new { success = false });
                response.Contents = s =>
                {
                    using (var writer = new System.IO.StreamWriter(new Nancy.IO.UnclosableStreamWrapper(s), System.Text.Encoding.UTF8))
                    {
                        writer.Write(sJson);
                    }
                };
                return response;
            }
            return null;
        }
    }
}