﻿using System.Collections.Generic;

namespace Little.Concur.Employees.Core.Data
{
    public class ConcurUserSearchResponse
    {
        public List<ConcurUser> Items { get; set; }
        public string NextPage { get; set; }
    }

    public class ConcurUser
    {
        public bool Active { get; set; }
        public string CellPhoneNumber { get; set; }
        public string EmployeeID { get; set; }
        public string FirstName { get; set; }
        public string ID { get; set; }
        public string LastName { get; set; }
        public string LoginID { get; set; }
        public string MiddleName { get; set; }
        public string OrganizationUnit { get; set; }
        public string PrimaryEmail { get; set; }
        public string URI { get; set; }
    }
}