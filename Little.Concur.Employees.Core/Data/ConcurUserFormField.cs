﻿namespace Little.Concur.Employees.Core.Data
{
    /// <summary>
    /// Configured fields on the Global employee form in Concur
    /// </summary>
    public class ConcurUserFormField
    {
        public string Id { get; set; }
        public string Label { get; set; }
        public string ControlType { get; set; }
        public string DataType { get; set; }
        public string MaxLength { get; set; }
        public string Required { get; set; }
        public string Cols { get; set; }
        public string Access { get; set; }
        public string Width { get; set; }
        public string Custom { get; set; }
        public string Sequence { get; set; }

        // Custom fields only
        public string ParentFormTypeCode { get; set; }
        public string ParentFieldId { get; set; }
        public string IsCopyDownSourceForOtherForms { get; set; }
        public string ListName { get; set; }
        public string HierLevel { get; set; }
    }
}