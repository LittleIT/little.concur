﻿using System.Collections.Generic;

namespace Little.Concur.Employees.Core.Data
{
    public class ConcurUserSaveResponse
    {
        public int RecordsSucceeded { get; set; }
        public int RecordsFailed { get; set; }
        public List<ConcurBatchResponseError> Errors { get; set; }
        public List<ConcurBatchResponseUserInfo> UserDetails { get; set; }
    }

    public class ConcurBatchResponseError
    {
        public string EmployeeID { get; set; }
        public int FeedRecordNumber { get; set; }
        public string Message { get; set; }
    }

    public class ConcurBatchResponseUserInfo
    {
        public string EmployeeID { get; set; }
        public int FeedRecordNumber { get; set; }
        public string Status { get; set; }
    }
}