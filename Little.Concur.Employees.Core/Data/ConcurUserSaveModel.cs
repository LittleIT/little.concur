﻿using System.Collections.Generic;
using System.Xml.Serialization;
using System;

namespace Little.Concur.Employees.Core.Data
{
    /// <summary>
    /// Users are saved in batches up to 500 entries, including updates and creates. There are no deletions.
    /// https://developer.concur.com/api-reference/user/
    /// </summary>
    [Serializable()]
    [XmlType(AnonymousType = true, Namespace = "http://www.concursolutions.com/api/user/2011/02")]
    [XmlRoot(Namespace = "http://www.concursolutions.com/api/user/2011/02", IsNullable = false)]
    public class batch
    {
        [XmlElement("UserProfile")]
        public List<BatchUserProfile> UserProfiles { get; set; }
    }

    [Serializable()]
    [XmlType(AnonymousType = true, Namespace = "http://www.concursolutions.com/api/user/2011/02")]
    public class BatchUserProfile
    {
        /// <summary>
        /// Number in batch.
        /// </summary>
        public string FeedRecordNumber { get; set; }

        /// <summary>
        /// Login for Concur is email address
        /// </summary>
        public string LoginId { get; set; }

        /// <summary>
        /// BST employee id
        /// </summary>
        public string EmpId { get; set; }

        /// <summary>
        /// Flag for active status (Y/N)
        /// </summary>
        public string Active { get; set; }

        /// <summary>
        /// Local (e.g. "en_US")
        /// </summary>
        public string LocaleName { get; set; }

        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }

        /// <summary>
        /// Ledger (e.g. Default)
        /// </summary>
        public string LedgerKey { get; set; }

        /// <summary>
        /// EmployeeAdministrationCountry. Should be "US".
        /// </summary>
        public string Custom21 { get; set; }

        /// <summary>
        /// Country code, 2 characters (e.g. US)
        /// </summary>
        public string CtryCode { get; set; }
        /// <summary>
        /// Currency (e.g. USD)
        /// </summary>
        public string CrnKey { get; set; }
        /// < summary >
        /// Character codes for both country and state (e.g.US-NY)
        /// </summary>
        public string CtrySubCode { get; set; }

        /// <summary>
        /// Flag for permissions (Y/N)
        /// </summary>
        public string ExpenseUser { get; set; }
        /// <summary>
        /// Flag for permissions (Y/N)
        /// </summary>
        public string ExpenseApprover { get; set; }
        /// <summary>
        /// Flag for permissions (Y/N)
        /// </summary>
        public string TripUser { get; set; }
        /// <summary>
        /// Flag for permissions (Y/N)
        /// </summary>
        public string InvoiceUser { get; set; }
        /// <summary>
        /// Flag for permissions (Y/N)
        /// </summary>
        public string InvoiceApprover { get; set; }

        /// <summary>
        /// Approver should be listed before the user in the batch. If they don't have permission to be approver, API will return error.
        /// </summary>
        public string ExpenseApproverEmployeeID { get; set; }
    }
}