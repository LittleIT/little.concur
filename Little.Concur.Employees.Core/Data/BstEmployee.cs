﻿namespace Little.Concur.Employees.Core.Data
{
    /// <summary>
    /// Source employee data from BST
    /// </summary>
    public class BstEmployee
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string Status { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string OrgApprover { get; set; }
        public string CountryCode { get; set; }
        public string StateCode { get; set; }
        public string CurrencyCode { get; set; }
        public string LocaleName { get; set; }

        public bool IsActive { get { return Status == "A"; } }
    }
}