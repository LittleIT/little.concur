﻿namespace Little.Concur.Employees.Core.Data
{
    /// <summary>
    /// https://developer.concur.com/api-reference/user/
    /// </summary>
    public class ConcurUserGetModel
    {
        public string LoginId { get; set; }
        public string Active { get; set; } // Y/N
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mi { get; set; }
        public string EmailAddress { get; set; }
        public string EmpId { get; set; }
        public string LedgerName { get; set; }
        /// <summary>
        /// Not listed in API docs, but it is an optional baked-in field setup by Little within Concur. Real value is integer.
        /// </summary>
        public string LedgerKey { get; set; }
        public string LocaleName { get; set; }
        public string OrgUnit1 { get; set; }
        public string OrgUnit2 { get; set; }
        public string OrgUnit3 { get; set; }
        public string OrgUnit4 { get; set; }
        public string OrgUnit5 { get; set; }
        public string OrgUnit6 { get; set; }
        /// <summary>
        /// Phase - Project Structure - HierLevel 2
        /// </summary>
        public string Custom1 { get; set; }
        /// <summary>
        /// Task - Project Structure - HierLevel 3
        /// </summary>
        public string Custom2 { get; set; }
        /// <summary>
        /// Studio - Project Structure - HierLevel 4
        /// </summary>
        public string Custom3 { get; set; }
        /// <summary>
        /// Project - Project Structure - HierLevel 1
        /// </summary>
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
        public string Custom6 { get; set; }
        /// <summary>
        /// EmployeeAdministrationCountry - Employee Groups
        /// </summary>
        public string Custom21 { get; set; }

        public string CtryCode { get; set; }
        public string CashAdvanceAccountCode { get; set; }
        public string CrnKey { get; set; }
        public string CtrySubCode { get; set; }

        /// <summary>
        /// Y/N
        /// </summary>
        public string ExpenseUser { get; set; }
        /// <summary>
        /// Y/N
        /// </summary>
        public string ExpenseApprover { get; set; }
        /// <summary>
        /// Y/N
        /// </summary>
        public string TripUser { get; set; }
        /// <summary>
        /// Y/N
        /// </summary>
        public string InvoiceUser { get; set; }
        /// <summary>
        /// Y/N
        /// </summary>
        public string InvoiceApprover { get; set; }

        public string ExpenseApproverEmployeeID { get; set; }

        /// <summary>
        /// Y/N
        /// </summary>
        public string IsTestEmp { get; set; }

        public BatchUserProfile ToBatchUserProfile(int feedRecordNumber = 1)
        {
            return new BatchUserProfile
            {
                FeedRecordNumber = feedRecordNumber.ToString(),
                LoginId = this.EmailAddress,
                LocaleName = this.LocaleName,
                EmailAddress = this.EmailAddress,
                LedgerKey = "Default",
                EmpId = this.EmpId,
                Active = this.Active,
                Password = ConcurEmployeeIntegrator.DefaultPassword,
                FirstName = this.FirstName,
                LastName = this.LastName,
                Custom21 = "US",
                CtryCode = this.CtryCode,
                CrnKey = this.CtryCode,
                CtrySubCode = this.CtrySubCode,
                ExpenseUser = this.ExpenseUser,
                ExpenseApprover = this.ExpenseApprover,
                ExpenseApproverEmployeeID = this.ExpenseApproverEmployeeID,
                TripUser = this.TripUser
            };
        }
    }
}