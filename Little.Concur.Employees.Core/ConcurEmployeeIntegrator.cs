﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Little.Concur.Employees.Core.Data;
using RestSharp;
using Serilog;

namespace Little.Concur.Employees.Core
{
    /// <summary>
    /// Synchronize employee data from the source BST system via the Concur APIs
    /// </summary>
    public class ConcurEmployeeIntegrator
    {
        internal const string DefaultPassword = "welcome123";

        private readonly ConcurSettings _settings;
        private readonly IConcurTokenManager _tokenManager;
        private readonly string _connection;
        private readonly ILogger _log;
        private readonly RestClient _client;

        public ConcurEmployeeIntegrator(ConcurSettings settings, string connection)
        {
            _settings = settings;
            _connection = connection;
            _tokenManager = new ConcurTokenManager(_settings);
            _log = Log.ForContext<ConcurEmployeeIntegrator>();
            if (!_settings.IsDryRun || !string.IsNullOrWhiteSpace(_settings.ConcurUrl))
            {
                _client = new RestClient(_settings.ConcurUrl);
            }
        }

        public void Execute()
        {
            var sourceEmployees = GetBstEmployees();
            Log.Debug("BST employees: {@SourceEmployees}", sourceEmployees);
            Log.Debug("BST active {Count}", sourceEmployees.Count(x => x.IsActive));
            Log.Debug("BST inactive {Count}", sourceEmployees.Count(x => !x.IsActive));
            var concurUsers = GetConcurUsers();
            Log.Debug("Concur Users: {@ConcurUsers}", concurUsers);
            Log.Debug("Concur Active {Count}", concurUsers.Count(x => x.Active));
            Log.Debug("Concur Inactive {Count}", concurUsers.Count(x => !x.Active));

            // If admins add/update custom fields within Concur, the new settings can be retrieved via API.
            // The save model will then need to be updated accordingly. The results for the current code is in Data\fields.json
            //var fields = GetConcurUserFormFields();
            //Log.Debug("{@Fields}", fields);

            // Useful debugging logic for comparing search results vs. full detail single get 
            //var loginId = "shensey@littleonline.com";
            //var userSimple = concurUsers.FirstOrDefault(x => x.LoginID.Equals(loginId, System.StringComparison.InvariantCultureIgnoreCase));
            //var userDetailed = GetConcurUser(loginId);

            var models = CreateUserSaveModels(sourceEmployees, concurUsers, 500);
            // TODO Remove before releasing to production. Temporary until they figure out account issue for specific user.
            models.RemoveAll(x => x.LoginId == "shensey@littleonline.com");
            Save(models);
        }

        /// <summary>
        /// Please note that the search API is v3 and only returns a subset of fields.
        /// </summary>
        /// <param name="models"></param>
        private void Save(List<BatchUserProfile> models)
        {
            const int MaxBatchSize = 500;
            if (models == null || !models.Any())
            {
                _log.Debug("No user changes to save");
                return;
            }
            if (_settings.IsDryRun)
            {
                _log.Debug("Dry run. Not saving anything.");
                return;
            }
            _log.Debug("Attempting to save new and modified users {@Models}", models);
            int iBatch = 0;
            var batchModels = models.Take(MaxBatchSize);
            while (batchModels.Any())
            {
                var request = new RestRequest($"/api/user/v1.0/users", Method.POST);
                request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
                var batch = new batch
                {
                    UserProfiles = batchModels.ToList()
                };
                request.XmlSerializer = new DotNetXmlSerializer(_log, Serilog.Events.LogEventLevel.Information);
                request.AddXmlBody(batch, "http://www.concursolutions.com/api/user/2011/02");
                var response = _client.Execute<ConcurUserSaveResponse>(request);
                if (ConcurApiHelper.HasRestError(_log, response))
                {
                    _log.Debug("Short circuiting out of save logic due to REST error");
                    return;
                }
                if (response.Data == null)
                {
                    _log.Error("No response data. Short-circuiting out. {@Content}", response.Content);
                    return;
                }
                if (response.Data.RecordsFailed > 0)
                {
                    if (response.Data.Errors != null)
                    {
                        _log.Error("Errors saving users. {@Errors}", response.Data.Errors);
                    }
                    else
                    {
                        _log.Error("Errors saving users: {Content}", response.Content);
                    }
                }
                if (response.Data.RecordsSucceeded > 0)
                {
                    if (response.Data.UserDetails != null)
                    {
                        _log.Information("Saved {Count} users: {@UserDetails}", response.Data.UserDetails.Count, response.Data.UserDetails);
                    }
                    else
                    {
                        _log.Information("Saved users: {Content}", response.Content);
                    }
                }
                iBatch++;
                batchModels = models.Skip(iBatch * MaxBatchSize).Take(MaxBatchSize);
            }
        }

        public List<BatchUserProfile> CreateUserSaveModels(IEnumerable<BstEmployee> sourceEmployees, IEnumerable<ConcurUser> concurUsers, int maxCount = 500)
        {
            _log.Debug("Converting BST data to Concur format");
            var models = new List<BatchUserProfile>();
            if (sourceEmployees == null || !sourceEmployees.Any())
            {
                return models;
            }
            var modified =
                from e in sourceEmployees
                join c in concurUsers on e.Code equals c.EmployeeID
                where e.IsActive != c.Active
                select new { Source = e, Existing = c };

            int i = 0;
            foreach (var m in modified)
            {
                i++;
                if (i > maxCount)
                {
                    break;
                }
                // Forced to include all required fields for updates
                // Utilizing existing Concur data where possible, as this is primarily for inactivating accounts when employees are no longer with the firm.
                var data = new BatchUserProfile
                {
                    FeedRecordNumber = i.ToString(),
                    LoginId = m.Existing.LoginID,
                    EmpId = m.Existing.EmployeeID,
                    Active = m.Source.IsActive ? "Y" : "N",
                    Password = DefaultPassword,
                    LocaleName = m.Source.LocaleName,
                    EmailAddress = m.Existing.PrimaryEmail,
                    LedgerKey = "Default",
                    FirstName = m.Existing.FirstName,
                    LastName = m.Existing.LastName,
                    Custom21 = "US",
                    CtryCode = m.Source.CountryCode,
                    CrnKey = m.Source.CurrencyCode,
                    CtrySubCode = $"{m.Source.CountryCode}-{m.Source.StateCode}",
                    ExpenseUser = "Y",
                    TripUser = "Y"
                };
                models.Add(data);
            }
            var newApprover = new Dictionary<string, List<string>>();
            var newUsers = sourceEmployees.Where(x => x.IsActive && !concurUsers.Any(z => x.Code == z.EmployeeID)).ToList();
            foreach (var n in newUsers)
            {
                i++;
                if (i > maxCount)
                {
                    break;
                }
                var data = new BatchUserProfile
                {
                    FeedRecordNumber = i.ToString(),
                    LoginId = n.Email,
                    LocaleName = n.LocaleName,
                    EmailAddress = n.Email,
                    LedgerKey = "Default",
                    EmpId = n.Code,
                    Active = n.IsActive ? "Y" : "N",
                    Password = DefaultPassword,
                    FirstName = n.FirstName,
                    LastName = n.LastName,
                    Custom21 = "US",
                    CtryCode = n.CountryCode,
                    CrnKey = n.CurrencyCode,
                    CtrySubCode = $"{n.CountryCode}-{n.StateCode}",
                    ExpenseUser = "Y",
                    TripUser = "Y"
                };
                // Approvers need to be in the batch prior to the employee. Primarily impacts scenario where both approver and employee are new.
                if (n.OrgApprover.IsNotEmpty() && n.Code != n.OrgApprover)
                {
                    if (concurUsers.Any(x => x.EmployeeID == n.OrgApprover))
                    {
                        _log.Debug("Setting approver Id {OrgApprover} for emp id {Code}", n.OrgApprover, n.Code);
                        data.ExpenseApproverEmployeeID = n.OrgApprover;
                    }
                    else
                    {
                        if (newApprover.TryGetValue(n.OrgApprover, out var values))
                        {
                            _log.Debug("Appending approver Id {OrgApprover} for emp id {Code}", n.OrgApprover, n.Code);
                            values.Add(n.Code);
                        }
                        else
                        {
                            _log.Debug("Creating approver Id {OrgApprover} for emp id {Code}", n.OrgApprover, n.Code);
                            newApprover.Add(n.OrgApprover, new List<string> { n.Code });
                        }
                    }
                }
                models.Add(data);
            }
            foreach (var k in newApprover.Keys)
            {
                var approverIndex = models.FindIndex(x => x.EmpId == k);
                if (approverIndex < 0)
                {
                    // If approver doesn't have ExpenseApprover permission, the user save will fail. So append them to our batch if needed.
                    var approver = concurUsers.FirstOrDefault(x => x.EmployeeID == k);
                    if (approver == null)
                    {
                        continue;
                    }
                    var a = GetConcurUser(approver.LoginID);
                    if (a == null || a.ExpenseApprover == "Y")
                    {
                        continue;
                    }
                    _log.Debug("Adding approver {K} to batch since they're missing ExpenseApprover permission");
                    models.Add(a.ToBatchUserProfile(++i));
                }
                var lastRelatedIndex = models.FindLastIndex(x => newApprover[k].Contains(x.EmpId));
                // Sanity check
                if (lastRelatedIndex < 0)
                {
                    _log.Debug("Skipping re-order logic as approver Id {K} has no one to approve in batch", k);
                    continue;
                }
                models[approverIndex].ExpenseApprover = "Y";
                if (approverIndex < lastRelatedIndex)
                {
                    _log.Debug("Swapping index {ApproverIndex} for approver Id {K} with last related employee index {LastRelatedIndex} in batch", approverIndex, k, lastRelatedIndex);
                    var a = models[approverIndex];

                    var feedIndex = a.FeedRecordNumber;
                    models[approverIndex].FeedRecordNumber = models[lastRelatedIndex].FeedRecordNumber;
                    models[lastRelatedIndex].FeedRecordNumber = feedIndex;

                    models[approverIndex] = models[lastRelatedIndex];
                    models[lastRelatedIndex] = a;
                    approverIndex = lastRelatedIndex;
                }
                foreach (var e in models.FindAll(x => newApprover[k].Contains(x.EmpId)))
                {
                    e.ExpenseApproverEmployeeID = k;
                }
            }
            return models;
        }

        /// <summary>
        /// The API docs only mark fields that are required by Concur itself. This lookup retrieves all fields setup for the company, including any built-in and custom
        /// fields that are required.
        /// </summary>
        private List<ConcurUserFormField> GetConcurUserFormFields()
        {
            _log.Debug("Retrieving lists from Concur API");
            _client.Authenticator = _tokenManager.GetAuthenticator(_client);
            var request = new RestRequest("/api/user/v1.0/FormFields", Method.GET);
            request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
            var response = _client.Execute<List<ConcurUserFormField>>(request);
            if (ConcurApiHelper.HasRestError(_log, response) || response.Data == null)
            {
                return null;
            }
            return response.Data;
        }

        /// <summary>
        /// The Concur API search only returns a subset of fields; if you need all fields the singular get method has to be used.
        /// Only works for active users.
        /// </summary>
        private ConcurUserGetModel GetConcurUser(string loginID)
        {
            _log.Debug("Retrieving single employee from Concur API");
            _client.Authenticator = _tokenManager.GetAuthenticator(_client);
            var request = new RestRequest("/api/user/v1.0/user", Method.GET);
            request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
            request.AddParameter("loginID", loginID);
            var response = _client.Execute<ConcurUserGetModel>(request);
            if (ConcurApiHelper.HasRestError(_log, response) || response.Data == null)
            {
                return null;
            }
            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                _log.Error($"Bad request {response.Content}");
                return null;
            }
            return response.Data;
        }

        /// <summary>
        /// Returns a subset of fields for all users in Concur for the company
        /// </summary>
        private List<ConcurUser> GetConcurUsers()
        {
            _log.Debug("Retrieving existing users from Concur API");
            _client.Authenticator = _tokenManager.GetAuthenticator(_client);
            var items = new List<ConcurUser>();
            string url = "/api/v3.0/common/users";
            while (!string.IsNullOrWhiteSpace(url))
            {
                var request = new RestRequest(url, Method.GET);
                request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
                if (!url.Contains("limit"))
                {
                    request.AddParameter("limit", 100);
                }
                var response = _client.Execute<ConcurUserSearchResponse>(request);
                if (ConcurApiHelper.HasRestError(_log, response))
                {
                    return null;
                }
                if (response.Data != null && response.Data.Items.Any())
                {
                    items.AddRange(response.Data.Items);
                    // RestSharp includes the root part of Url in client instance, so it needs to be stripped to avoid invalid request route
                    url = string.IsNullOrWhiteSpace(response.Data.NextPage) ? null : response.Data.NextPage.Replace(_settings.ConcurUrl, "");
                }
                else
                {
                    url = null;
                }
            }
            return items;
        }

        /// <summary>
        /// Retrieve all employees from BST that should (or could) be users in Concur.
        /// Since Concur login is email, we filter out anyone without one.
        /// </summary>
        /// <returns></returns>
        private IEnumerable<BstEmployee> GetBstEmployees()
        {
            _log.Debug("Retrieving source data from BST database");
            var sql = @"select
	            Code = e.emp_code, 
	            [Name] = e.emp_name, 
	            LastName = substring(e.emp_name, 1, charindex(',',e.emp_name, 1) - 1), 
	            FirstName = substring(e.emp_name, charindex(',',e.emp_name, 1) + 2, LEN(e.emp_name)), 
	            [Status] = e.rec_status_ind,
	            Phone = c.voice_phone_nbr, 
	            Email = c.email_addr,
				OrgApprover = o.emp_code,
				CountryCode = 'US',
				StateCode =
					case substring(e.org_code, 2, 1) 
			            when '2' then 'VA' 
			            when '3' then 'CA' 
			            when '5' then 'NC' 
			            when '6' then 'CA' 
			            when '8' then 'FL'
			            else 'NC'
		            end,
				CurrencyCode = 'USD',
				LocaleName = 'en_US'
            from dbo.emp_info e with (nolock)
	            inner join dbo.emp_correspond c with (nolock) on e.emp_code = c.emp_code
				inner join dbo.org_code o ON e.org_code = o.org_code AND e.co_code = o.co_code
            where
	            e.emp_code <> '******'
	            and e.emp_name like '%,%'
	            and c.email_addr is not null
				and o.org_status_ind = 'A'";
            using (var con = new SqlConnection(_connection))
            {
                con.Open();
                return con.Query<BstEmployee>(sql);
            }
        }
    }
}