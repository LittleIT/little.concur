﻿namespace Little.Concur.Lists.Core.Data
{
    public class ConcurListItemCreateModel
    {
        public string Name { get; set; }
        public string ListID { get; set; }
        public string Level1Code { get; set; }
        public string Level2Code { get; set; }
        public string Level3Code { get; set; }
        public string Level4Code { get; set; }
    }
}