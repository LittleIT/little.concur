﻿using System;
using System.Collections.Generic;

namespace Little.Concur.Lists.Core.Data
{
    public class ConcurListItemContainer
    {
        public List<ConcurListItem> Items { get; set; }
        public string NextPage { get; set; }
    }

    /// <summary>
    /// Equatable checks are against ListID and Levels 1-4 (project > phase > task > studio). All other fields ignored.
    /// </summary>
    public class ConcurListItem : IEquatable<ConcurListItem>
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string ListID { get; set; }
        public string ParentID { get; set; }
        public string Level1Code { get; set; }
        public string Level2Code { get; set; }
        public string Level3Code { get; set; }
        public string Level4Code { get; set; }
        public string Level5Code { get; set; }
        public string Level6Code { get; set; }
        public string Level7Code { get; set; }
        public string Level8Code { get; set; }
        public string Level9Code { get; set; }
        public string Level10Code { get; set; }
        public string URI { get; set; }

        public string Hierarchy { get { return $"{Level1Code}-{Level2Code}-{Level3Code}-{Level4Code}"; } }

        public bool Equals(ConcurListItem other)
        {
            if (Object.ReferenceEquals(other, null))
            {
                return false;
            }
            if (Object.ReferenceEquals(this, other))
            {
                return true;
            }
            return
                ListID.SafeEquals(other.ListID)
                && Level1Code.SafeEquals(other.Level1Code)
                && Level2Code.SafeEquals(other.Level2Code)
                && Level3Code.SafeEquals(other.Level3Code)
                && Level4Code.SafeEquals(other.Level4Code);
        }

        public override int GetHashCode()
        {
            return ListID.SafeHashCode() ^ Level1Code.SafeHashCode() ^ Level2Code.SafeHashCode() ^ Level3Code.SafeHashCode() ^ Level4Code.SafeHashCode();
        }
    }
}