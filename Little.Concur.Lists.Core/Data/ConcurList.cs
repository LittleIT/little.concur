﻿using System.Collections.Generic;

namespace Little.Concur.Lists.Core.Data
{
    public class ConcurListContainer
    {
        public List<ConcurList> Items { get; set; }
        public string NextPage { get; set; }
    }

    public class ConcurList
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string ConnectorID { get; set; }
        public bool DisplayCodeFirst { get; set; }
        public string ExternalThreshold { get; set; }
        public bool IsVendorList { get; set; }
        public string SearchCriteriaCode { get; set; }
        public string URI { get; set; }
    }
}