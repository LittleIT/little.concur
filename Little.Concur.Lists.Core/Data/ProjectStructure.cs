﻿namespace Little.Concur.Lists.Core.Data
{
    public class ProjectStructure
    {
        public string ProjectCode { get; set; }
        public string ProjectName { get; set; }
        public string PhaseCode { get; set; }
        public string PhaseName { get; set; }
        public string TaskCode { get; set; }
        public string TaskName { get; set; }
        public string StudioCode { get; set; }
        public string StudioName { get; set; }
    }
}