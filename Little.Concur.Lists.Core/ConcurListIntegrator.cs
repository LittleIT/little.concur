﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Little.Concur.Lists.Core.Data;
using RestSharp;
using Serilog;

namespace Little.Concur.Lists.Core
{
    public class ConcurListIntegrator
    {
        public const string ProjectStructureListId = "gWtIeOnI3U6NDgAOjTpjbFiJywPJZfJSedg";

        private readonly ConcurSettings _settings;
        private readonly IConcurTokenManager _tokenManager;
        private readonly string _connection;
        private readonly ILogger _log;
        private readonly RestClient _client;

        public ConcurListIntegrator(ConcurSettings settings, string connection)
        {
            _settings = settings;
            _connection = connection;
            _tokenManager = new ConcurTokenManager(_settings);
            _log = Log.ForContext<ConcurListIntegrator>();
            if (!_settings.IsDryRun || _settings.ConcurUrl.IsNotEmpty())
            {
                _client = new RestClient(_settings.ConcurUrl);
            }
        }

        public void Execute()
        {
            var bstData = GetBstHierarchy();
            var source = ConvertBstToConcur(bstData);
            var existing = GetConcurListItems(ProjectStructureListId);
            SaveNewItems(source, existing);
            DeleteOldItems(source, existing);
            SaveNameChanges(source, existing);
        }

        private void SaveNewItems(IEnumerable<ConcurListItem> source, IEnumerable<ConcurListItem> existing, int maxProjectsToSave = int.MaxValue)
        {
            var newItems = source.Except(existing);
            _log.Debug("Found {Count} new items. Will at most process {MaxProjectsToSave}", newItems.Count(), maxProjectsToSave);
            if (_settings.IsDryRun)
            {
                return;
            }
            HashSet<string> projectsSaved = new HashSet<string>();
            foreach (var item in newItems)
            {
                if (projectsSaved.Count > maxProjectsToSave)
                {
                    _log.Debug("Short-circuiting out as we've processed {MaxProjectsToSave} new items", maxProjectsToSave);
                    break;
                }
                var request = new RestRequest($"/api/v3.0/common/listitems", Method.POST);
                request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
                var model = new ConcurListItemCreateModel
                {
                    Level1Code = item.Level1Code,
                    Level2Code = item.Level2Code,
                    Level3Code = item.Level3Code,
                    Level4Code = item.Level4Code,
                    ListID = ProjectStructureListId,
                    Name = item.Name
                };
                request.AddJsonBody(model);
                var response = _client.Execute(request);
                if (ConcurApiHelper.HasRestError(_log, response))
                {
                    _log.Debug("Short circuiting out of save new items logic due to REST error");
                    break;
                }
                _log.Information("New item saved to API {@Model}", model);
                if (!projectsSaved.Contains(model.Level1Code))
                {
                    projectsSaved.Add(model.Level1Code);
                }
            }
        }

        private void DeleteOldItems(IEnumerable<ConcurListItem> source, IEnumerable<ConcurListItem> existing)
        {
            var oldItems = existing.Except(source);
            _log.Debug("Found {Count} old items: {@OldItems}", oldItems.Count(), oldItems);
            if (_settings.IsDryRun)
            {
                return;
            }
            foreach (var old in oldItems)
            {
                // As per Concur docs, this performs a soft delete, only doing a full delete if the item was never used
                var request = new RestRequest($"/api/v3.0/common/listitems/{old.ID}?listId={old.ListID}", Method.DELETE);
                request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
                //request.AddHeader("Content-Type", "application/x-www-form-urlencoded"); //application/json
                //request.AddParameter("listId", old.ListID);
                var response = _client.Execute(request);
                if (ConcurApiHelper.HasRestError(_log, response))
                {
                    _log.Debug("Short circuiting out of delete logic due to error");
                    break;
                }
                _log.Information("Item deleted {@Old}", old);
            }
        }

        private void SaveNameChanges(IEnumerable<ConcurListItem> source, IEnumerable<ConcurListItem> existing)
        {
            var both = source.Intersect(existing);
            _log.Debug("Found {Count} in both BST and Concur", both.Count());
            var nameChanges =
                from b in both
                join e in existing on b.Hierarchy equals e.Hierarchy
                where b.Name != e.Name
                select new
                {
                    Id = e.ID,
                    ExistingName = e.Name,
                    BstName = b.Name,
                    Hierarchy = e.Hierarchy,
                    Level1Code = e.Level1Code,
                    Level2Code = e.Level2Code,
                    Level3Code = e.Level3Code,
                    Level4Code = e.Level4Code
                };
            _log.Debug("Found {Count} names that need to be changed: {@Named}", nameChanges.Count(), nameChanges);
            if (_settings.IsDryRun)
            {
                return;
            }
            foreach (var nameChange in nameChanges)
            {
                var request = new RestRequest($"/api/v3.0/common/listitems/{nameChange.Id}", Method.PUT);
                request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
                var model = new ConcurListItemUpdateModel
                {
                    Level1Code = nameChange.Level1Code,
                    Level2Code = nameChange.Level2Code,
                    Level3Code = nameChange.Level3Code,
                    Level4Code = nameChange.Level4Code,
                    ListID = ProjectStructureListId,
                    Name = nameChange.BstName
                };
                request.AddJsonBody(model);
                var response = _client.Execute(request);
                if (ConcurApiHelper.HasRestError(_log, response))
                {
                    _log.Debug("Short circuiting out of name change logic due to REST error");
                    break;
                }
                _log.Information("Name change saved to API for {@NameChange}", nameChange);
            }
        }

        private IEnumerable<ConcurList> GetConcurLists()
        {
            _log.Debug("Retrieving lists from Concur API");
            _client.Authenticator = _tokenManager.GetAuthenticator(_client);
            var request = new RestRequest("/api/v3.0/common/lists", Method.GET);
            request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
            var response = _client.Execute<ConcurListContainer>(request);
            if (ConcurApiHelper.HasRestError(_log, response) || response.Data == null)
            {
                return null;
            }
            return response.Data.Items;
        }

        private List<ConcurListItem> GetConcurListItems(string listId = null)
        {
            _log.Debug("Retrieving existing list items from Concur API");
            _client.Authenticator = _tokenManager.GetAuthenticator(_client);
            var items = new List<ConcurListItem>();
            string url = "/api/v3.0/common/listitems";
            while (!string.IsNullOrWhiteSpace(url))
            {
                var request = new RestRequest(url, Method.GET);
                request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
                if (!url.Contains("limit"))
                {
                    request.AddParameter("limit", 100);
                }
                if (listId != null && !url.Contains("listId"))
                {
                    request.AddParameter("listId", listId);
                }
                var response = _client.Execute<ConcurListItemContainer>(request);
                if (ConcurApiHelper.HasRestError(_log, response))
                {
                    return null;
                }
                if (response.Data != null && response.Data.Items.Any())
                {
                    items.AddRange(response.Data.Items);
                    // RestSharp includes the root part of Url in client instance, so it needs to be stripped to avoid invalid request route
                    url = string.IsNullOrWhiteSpace(response.Data.NextPage) ? null : response.Data.NextPage.Replace(_settings.ConcurUrl, "");
                }
                else
                {
                    url = null;
                }
            }
            return items;
        }

        public List<ConcurListItem> ConvertBstToConcur(IEnumerable<ProjectStructure> bst)
        {
            _log.Debug("Converting BST data to Concur format");
            var items = new List<ConcurListItem>();
            if (bst == null || !bst.Any())
            {
                return items;
            }
            foreach (var data in bst)
            {
                items.Add(new ConcurListItem
                {
                    ListID = ConcurListIntegrator.ProjectStructureListId,
                    Level1Code = data.ProjectCode,
                    Name = data.ProjectName,
                    Level2Code = "",
                    Level3Code = "",
                    Level4Code = ""
                });
                items.Add(new ConcurListItem
                {
                    ListID = ConcurListIntegrator.ProjectStructureListId,
                    Level1Code = data.ProjectCode,
                    Level2Code = data.PhaseCode,
                    Name = data.PhaseName,
                    Level3Code = "",
                    Level4Code = ""
                });
                items.Add(new ConcurListItem
                {
                    ListID = ConcurListIntegrator.ProjectStructureListId,
                    Level1Code = data.ProjectCode,
                    Level2Code = data.PhaseCode,
                    Level3Code = data.TaskCode,
                    Name = data.TaskName,
                    Level4Code = ""
                });
                items.Add(new ConcurListItem
                {
                    ListID = ConcurListIntegrator.ProjectStructureListId,
                    Level1Code = data.ProjectCode,
                    Level2Code = data.PhaseCode,
                    Level3Code = data.TaskCode,
                    Level4Code = data.StudioCode,
                    Name = data.StudioName
                });
            }
            return items;
        }

        private IEnumerable<ProjectStructure> GetBstHierarchy()
        {
            _log.Debug("Retrieving source data from BST database");
            using (var con = new SqlConnection(_connection))
            {
                var sql = @"select
                    ProjectCode = p.prj_code,
                    ProjectName = (select prj_name from prj_info where prj_code = p.prj_code),
                    PhaseCode = ph.phase_code,
                    PhaseName = (select phase_name from prj_phase where prj_code = p.prj_code and phase_code = ph.phase_code),
	                TaskCode = b.task_code,
	                TaskName = (select task_name from task_code where task_code = b.task_code),
                    StudioCode = b.org_code,
	                StudioName = (select org_name from org_code where org_code = b.org_code)
                from dbo.prj_processing p
                    inner join dbo.prj_phase_processing ph on p.prj_code = ph.prj_code
                    inner join dbo.prj_rev_bdgt b on ph.prj_code = b.prj_code AND ph.phase_code = b.phase_code
                where p.prj_status_ind = 'a'
                    AND ph.phase_code in ('g11', 'n20', '20', 'r20', 'M13', '03exp', 'R21', 'r20cd')
                    AND ph.phase_report_status = 'a'
                    AND (b.reg_input_ind = 'E' OR b.reg_input_ind = 'B') 
                    AND (p.reg_input_ind = 'E' OR  p.reg_input_ind = 'B')
                group by p.prj_code, ph.phase_code, b.task_code, b.org_code
                order by p.prj_code, ph.phase_code, b.task_code, b.org_code";
                con.Open();
                return con.Query<ProjectStructure>(sql);
            }
        }
    }
}