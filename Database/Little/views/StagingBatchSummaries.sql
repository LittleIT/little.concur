if object_id('StagingBatchSummaries') is null
begin
	declare @sql varchar(max)
	set @sql = 'create view dbo.StagingBatchSummaries as select [null] = null'
	exec (@sql)
end
go

alter view dbo.StagingBatchSummaries

as

select
	Id,
	[Name],
	Amount,
	[Count],
	IsClosed,
	[Type],
	PaymentMethod,
	BatchUrl,
	Retrieved,
	StagedJournalEntryCount = (select count(*) from StagingJournalEntry where ConcurBatchID = Id),
	CompletedJournalEntryCount = (select count(*) from StagingJournalEntry where ConcurBatchID = Id and Completed is not null)
from StagingBatchSummary
