SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StagingBatchSummary](
	[Id] [varchar](50) NOT NULL,
	[Name] [varchar](200) NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Count] [int] NOT NULL,
	[IsClosed] [bit] NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[PaymentMethod] [varchar](100) NOT NULL,
	[BatchUrl] [varchar](max) NOT NULL,
	[Retrieved] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_StagingBatchSummary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

ALTER TABLE [dbo].[StagingBatchSummary] ADD  CONSTRAINT [DF_StagingBatchSummary_IsClosed]  DEFAULT ((0)) FOR [IsClosed]
GO
