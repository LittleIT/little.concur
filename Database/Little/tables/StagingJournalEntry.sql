SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[StagingJournalEntry](
	[batch_skey] [char](16) NOT NULL,
	[co_code] [varchar](4) NULL,
	[usr_batch_id] [varchar](3) NULL,
	[per_end_date] [datetime] NULL,
	[sys_doc_type_code] [varchar](2) NULL,
	[usr_doc_type_suf] [varchar](2) NULL,
	[max_dcml_places] [smallint] NULL,
	[input_date] [datetime] NULL,
	[batch_doc_total] [smallint] NULL,
	[batch_acct_db_total] [numeric](28, 9) NULL,
	[batch_acct_cr_total] [numeric](28, 9) NULL,
	[batch_co_db_total] [numeric](28, 9) NULL,
	[batch_co_cr_total] [numeric](28, 9) NULL,
	[seq_nbr] [int] NOT NULL,
	[doc_nbr] [varchar](10) NULL,
	[doc_date] [datetime] NULL,
	[unit_input_yn] [char](1) NULL,
	[currency_code] [varchar](5) NULL,
	[currency_rate] [numeric](28, 9) NULL,
	[reversal_yn] [char](1) NULL,
	[reversal_per_end_date] [datetime] NULL,
	[doc_acct_db_total] [numeric](28, 9) NULL,
	[doc_acct_cr_total] [numeric](28, 9) NULL,
	[doc_co_db_total] [numeric](28, 9) NULL,
	[doc_co_cr_total] [numeric](28, 9) NULL,
	[line_nbr] [int] NOT NULL,
	[trn_co_code] [varchar](4) NULL,
	[org_code] [varchar](8) NULL,
	[acct_code] [varchar](10) NULL,
	[evc_type_code] [char](1) NULL,
	[evc_code] [varchar](9) NULL,
	[extern_ref_nbr] [varchar](15) NULL,
	[extern_ref_date] [datetime] NULL,
	[intern_ref_nbr] [varchar](10) NULL,
	[trn_date] [datetime] NULL,
	[wbs_ind] [char](1) NULL,
	[db_amt_ac] [numeric](28, 9) NULL,
	[cr_amt_ac] [numeric](28, 9) NULL,
	[db_amt_cc] [numeric](28, 9) NULL,
	[cr_amt_cc] [numeric](28, 9) NULL,
	[prj_code] [varchar](10) NULL,
	[phase_code] [varchar](6) NULL,
	[task_code] [varchar](6) NULL,
	[inv_grouping_code] [varchar](2) NULL,
	[actv_code] [varchar](4) NULL,
	[rsrc_code] [varchar](6) NULL,
	[qty_amt] [numeric](28, 9) NULL,
	[pcs_desc] [varchar](255) NULL,
	[fcs_desc] [varchar](255) NULL,
	ConcurBatchID varchar(40) not null,
	StageId uniqueidentifier not null,
	Staged datetimeoffset not null,
	Completed datetimeoffset null,
 CONSTRAINT [PK_StagingJournalEntry__batch_skey__seq_nbr__line_nbr] PRIMARY KEY CLUSTERED 
(
	[batch_skey] ASC,
	[seq_nbr] ASC,
	[line_nbr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__co_code]  DEFAULT (null) FOR [co_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__usr_batch_id]  DEFAULT (null) FOR [usr_batch_id]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__per_end_date]  DEFAULT (null) FOR [per_end_date]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__sys_doc_type_code]  DEFAULT (null) FOR [sys_doc_type_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__usr_doc_type_suf]  DEFAULT (null) FOR [usr_doc_type_suf]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__max_dcml_places]  DEFAULT (null) FOR [max_dcml_places]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__input_date]  DEFAULT (null) FOR [input_date]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__batch_doc_total]  DEFAULT (null) FOR [batch_doc_total]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__doc_nbr]  DEFAULT (null) FOR [doc_nbr]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__doc_date]  DEFAULT (null) FOR [doc_date]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__unit_input_yn]  DEFAULT (null) FOR [unit_input_yn]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__currency_code]  DEFAULT (null) FOR [currency_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__reversal_yn]  DEFAULT (null) FOR [reversal_yn]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__reversal_per_end_date]  DEFAULT (null) FOR [reversal_per_end_date]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__trn_co_code]  DEFAULT (null) FOR [trn_co_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__org_code]  DEFAULT (null) FOR [org_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__acct_code]  DEFAULT (null) FOR [acct_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__evc_type_code]  DEFAULT (null) FOR [evc_type_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__evc_code]  DEFAULT (null) FOR [evc_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__extern_ref_nbr]  DEFAULT (null) FOR [extern_ref_nbr]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__extern_ref_date]  DEFAULT (null) FOR [extern_ref_date]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__intern_ref_nbr]  DEFAULT (null) FOR [intern_ref_nbr]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__trn_date]  DEFAULT (null) FOR [trn_date]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__wbs_ind]  DEFAULT (null) FOR [wbs_ind]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__prj_code]  DEFAULT (null) FOR [prj_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__phase_code]  DEFAULT (null) FOR [phase_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__task_code]  DEFAULT (null) FOR [task_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__inv_grouping_code]  DEFAULT (null) FOR [inv_grouping_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__actv_code]  DEFAULT (null) FOR [actv_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__rsrc_code]  DEFAULT (null) FOR [rsrc_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry] ADD  CONSTRAINT [DF_StagingJournalEntry__pcs_desc]  DEFAULT (null) FOR [pcs_desc]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__acct_code] CHECK  ((rtrim(ltrim([acct_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__acct_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__actv_code] CHECK  ((rtrim(ltrim([actv_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__actv_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__co_code] CHECK  ((rtrim(ltrim([co_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__co_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__currency_code] CHECK  ((rtrim(ltrim([currency_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__currency_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__doc_nbr] CHECK  ((rtrim(ltrim([doc_nbr])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__doc_nbr]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__evc_code] CHECK  ((rtrim(ltrim([evc_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__evc_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__evc_type_code] CHECK  ((rtrim(ltrim([evc_type_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__evc_type_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__extern_ref_nbr] CHECK  ((rtrim(ltrim([extern_ref_nbr])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__extern_ref_nbr]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__intern_ref_nbr] CHECK  ((rtrim(ltrim([intern_ref_nbr])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__intern_ref_nbr]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__inv_grouping_code] CHECK  ((rtrim(ltrim([inv_grouping_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__inv_grouping_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__org_code] CHECK  ((rtrim(ltrim([org_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__org_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__pcs_desc] CHECK  ((rtrim(ltrim([pcs_desc])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__pcs_desc]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__phase_code] CHECK  ((rtrim(ltrim([phase_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__phase_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__prj_code] CHECK  ((rtrim(ltrim([prj_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__prj_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__reversal_yn] CHECK  ((rtrim(ltrim([reversal_yn])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__reversal_yn]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__rsrc_code] CHECK  ((rtrim(ltrim([rsrc_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__rsrc_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__sys_doc_type_code] CHECK  ((rtrim(ltrim([sys_doc_type_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__sys_doc_type_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__task_code] CHECK  ((rtrim(ltrim([task_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__task_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__trn_co_code] CHECK  ((rtrim(ltrim([trn_co_code])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__trn_co_code]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__unit_input_yn] CHECK  ((rtrim(ltrim([unit_input_yn])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__unit_input_yn]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__usr_batch_id] CHECK  ((rtrim(ltrim([usr_batch_id])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__usr_batch_id]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__usr_doc_type_suf] CHECK  ((rtrim(ltrim([usr_doc_type_suf])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__usr_doc_type_suf]
GO

ALTER TABLE [dbo].[StagingJournalEntry]  WITH NOCHECK ADD  CONSTRAINT [CK_StagingJournalEntry__wbs_ind] CHECK  ((rtrim(ltrim([wbs_ind])) <> ''))
GO

ALTER TABLE [dbo].[StagingJournalEntry] CHECK CONSTRAINT [CK_StagingJournalEntry__wbs_ind]
GO


