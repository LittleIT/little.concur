if object_id('ViewLogs') is null
begin
	declare @sql varchar(max)
	set @sql = 'create view dbo.ViewLogs as select [null] = null'
	exec (@sql)
end
go

alter view [dbo].[ViewLogs]

as

select
	Id,
	[Message],
	MessageTemplate,
	[Level],
	[TimeStamp],
	Exception,
	MachineName = [Properties].value('(//property[@key="MachineName"]/node())[1]', 'nvarchar(max)'),
	ProcessName = [Properties].value('(//property[@key="ProcessName"]/node())[1]', 'nvarchar(max)'),
	SourceContext = [Properties].value('(//property[@key="SourceContext"]/node())[1]', 'nvarchar(max)'),
	Properties,
	LogEvent
from Logs
