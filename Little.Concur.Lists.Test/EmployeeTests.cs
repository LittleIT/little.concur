﻿using System.Linq;
using Little.Concur.Employees.Core;
using Little.Concur.Employees.Core.Data;
using NUnit.Framework;

namespace Little.Concur.Lists.Test
{
    [TestFixture]
    public class EmployeeTests
    {
        [Test]
        public void GetUserCreateModel()
        {
            var sourceBst = new BstEmployee[] { new BstEmployee { Code = "1234", Email = "abc@example.com", FirstName = "Foo", LastName = "Bar", Status = "A", CountryCode = "US", StateCode = "AB" } };
            var g = new ConcurEmployeeIntegrator(new ConcurSettings { Environment = "Test", IsDryRun = true }, null);
            var result = g.CreateUserSaveModels(sourceBst, new ConcurUser[] { }, 1);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(sourceBst[0].Email, result.First().LoginId);
            var first = result.First();
            Assert.AreEqual($"US-{sourceBst[0].StateCode}", first.CtrySubCode);
        }

        [Test]
        public void GetUserUpdateModel()
        {
            var sourceBst = new BstEmployee[] { new BstEmployee { Code = "1234", Email = "abc@example.com", FirstName = "Foo", LastName = "Bar", Status = "A", CountryCode = "US", StateCode = "AB" } };
            var targetConcur = new ConcurUser[] { new ConcurUser { LoginID = sourceBst[0].Email, EmployeeID = sourceBst[0].Email, Active = false } };
            var g = new ConcurEmployeeIntegrator(new ConcurSettings { Environment = "Test", IsDryRun = true }, null);
            var result = g.CreateUserSaveModels(sourceBst, targetConcur, 1);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(sourceBst[0].Email, result.First().LoginId);
            Assert.AreEqual($"Y", result.First().Active);
        }

        [Test]
        public void GetUserReindexedModel()
        {
            string approverId = "1235";
            var sourceBst = new BstEmployee[]
            {
                new BstEmployee { Code = "1234", Email = "abc4@example.com", FirstName = "Foo4", LastName = "Bar4", Status = "A", CountryCode = "US", StateCode = "AB", OrgApprover = approverId },
                new BstEmployee { Code = approverId, Email = "abc5@example.com", FirstName = "Foo5", LastName = "Bar5", Status = "A", CountryCode = "US", StateCode = "AD" },
                new BstEmployee { Code = "1236", Email = "abc6@example.com", FirstName = "Foo6", LastName = "Bar6", Status = "A", CountryCode = "US", StateCode = "AE", OrgApprover = "9999" },
                new BstEmployee { Code = "1237", Email = "abc7@example.com", FirstName = "Foo7", LastName = "Bar7", Status = "A", CountryCode = "US", StateCode = "AF", OrgApprover = approverId }
            };
            var g = new ConcurEmployeeIntegrator(new ConcurSettings { Environment = "Test", IsDryRun = true }, null);
            var result = g.CreateUserSaveModels(sourceBst, new ConcurUser[] { });
            Assert.AreEqual(4, result.Count);
            Assert.AreEqual(approverId, result.Last().EmpId);
            Assert.AreNotEqual(approverId, result[1].EmpId);
            Assert.IsNull(result[2].ExpenseApproverEmployeeID);
            int i = -1;
            foreach (var r in result)
            {
                var feed = int.Parse(r.FeedRecordNumber);
                Assert.Greater(feed, i);
                i = feed;
            }
        }
    }
}