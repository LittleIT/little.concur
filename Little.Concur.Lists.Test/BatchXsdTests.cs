﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Schema;
using NUnit.Framework;

namespace Little.Concur.Lists.Test
{
    [TestFixture]
    public class BatchXsdTests
    {
        [Test]
        public void ValidateUpdateSampleXml()
        {
            var settings = new XmlReaderSettings();
            string xsdFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"Assets\batch-update.xsd");
            settings.Schemas.Add("http://www.concursolutions.com/api/user/2011/02", xsdFilePath);
            settings.ValidationType = ValidationType.Schema;
            string xmlFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"Assets\batch-update-sample.xml");
            XmlReader reader = XmlReader.Create(xmlFilePath, settings);
            var document = new XmlDocument();
            document.Load(reader);
            var eventHandler = new ValidationEventHandler(ValidationEventHandler);
            document.Validate(eventHandler);
        }

        [Test]
        public void ValidateInsertSampleXml()
        {
            var settings = new XmlReaderSettings();
            string xsdFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"Assets\batch-insert.xsd");
            settings.Schemas.Add("http://www.concursolutions.com/api/user/2011/02", xsdFilePath);
            settings.ValidationType = ValidationType.Schema;
            string xmlFilePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"Assets\batch-insert-sample.xml");
            XmlReader reader = XmlReader.Create(xmlFilePath, settings);
            var document = new XmlDocument();
            document.Load(reader);
            var eventHandler = new ValidationEventHandler(ValidationEventHandler);
            document.Validate(eventHandler);
        }

        static void ValidationEventHandler(object sender, ValidationEventArgs e)
        {
            switch (e.Severity)
            {
                case XmlSeverityType.Error:
                    Console.WriteLine("Error: {0}", e.Message);
                    break;
                case XmlSeverityType.Warning:
                    Console.WriteLine("Warning {0}", e.Message);
                    break;
            }
            Assert.AreNotEqual(XmlSeverityType.Error, e.Severity);
        }
    }
}