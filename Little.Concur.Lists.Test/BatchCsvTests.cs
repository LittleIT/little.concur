﻿using System.IO;
using Little.Concur.PaymentBatch.Core;
using NUnit.Framework;

namespace Little.Concur.Lists.Test
{
    [TestFixture]
    public class BatchCsvTests
    {
        [Test]
        public void LoadBatchCsv()
        {
            string csvFolderPath = Path.Combine(TestContext.CurrentContext.TestDirectory, "Assets");
            string csvFilePath = Path.Combine(csvFolderPath, @"Concur_Batch_11282017.csv");
            var results = ConcurPaymentBatchIntegrator.ProcessBatchFile(csvFilePath);
            Assert.IsNotNull(results);
            Assert.AreEqual(89, results.Count);
        }
    }
}