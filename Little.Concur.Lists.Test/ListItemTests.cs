﻿using System.Collections.Generic;
using System.Linq;
using Little.Concur.Lists.Core;
using Little.Concur.Lists.Core.Data;
using NUnit.Framework;

namespace Little.Concur.Lists.Test
{
    [TestFixture]
    public class ListItemTests
    {
        private const string ProjectFiller = "123456";
        private const string PhaseFiller = "789";
        private const string TaskFiller = "10";
        private const string StudioFiller = "abc";

        private List<ConcurListItem> SetupList(int startingIndex = 0, int projectCount = 1)
        {
            var list = new List<ConcurListItem>();
            for (int i = startingIndex; i < startingIndex + projectCount; i++)
            {
                list.Add(new ConcurListItem
                {
                    ListID = ConcurListIntegrator.ProjectStructureListId,
                    Level1Code = $"{ProjectFiller}{i}",
                    Name = $"{ProjectFiller}-{i}"
                });
                list.Add(new ConcurListItem
                {
                    ListID = ConcurListIntegrator.ProjectStructureListId,
                    Level1Code = $"{ProjectFiller}{i}",
                    Level2Code = $"{PhaseFiller}{i}",
                    Name = $"{PhaseFiller}-{i}"
                });
                list.Add(new ConcurListItem
                {
                    ListID = ConcurListIntegrator.ProjectStructureListId,
                    Level1Code = $"{ProjectFiller}{i}",
                    Level2Code = $"{PhaseFiller}{i}",
                    Level3Code = $"{TaskFiller}{i}",
                    Name = $"{TaskFiller}-{i}"
                });
                list.Add(new ConcurListItem
                {
                    ListID = ConcurListIntegrator.ProjectStructureListId,
                    Level1Code = $"{ProjectFiller}{i}",
                    Level2Code = $"{PhaseFiller}{i}",
                    Level3Code = $"{TaskFiller}{i}",
                    Level4Code = $"{StudioFiller}{i}",
                    Name = $"{StudioFiller}-{i}"
                });
            }
            Assert.AreEqual(projectCount * 4, list.Count, "SetupList failed miserably at its job");
            return list;
        }

        [Test]
        public void FilterNewItems()
        {
            var existing = SetupList(0, 5);
            var source = SetupList(0, 7);
            var newItems = source.Except(existing);
            Assert.AreEqual(8, newItems.Count());
        }

        [Test]
        public void FilterRemovedItems()
        {
            var existing = SetupList(0, 5);
            var source = SetupList(1, 3);
            var oldItems = existing.Except(source);
            Assert.AreEqual(8, oldItems.Count());
        }

        [Test]
        public void FilterMatchedItems()
        {
            var existing = SetupList(0, 3);
            var source = SetupList(0, 3);
            var both = source.Intersect(existing);
            Assert.AreEqual(source.Count, both.Count());
        }

        [Test]
        public void FilterUpdatedItems()
        {
            var existing = SetupList(0, 5);
            var source = SetupList(0, 5);
            source[2].Name = "New name 1";
            source[4].Name = "New name 2";
            var both = source.Intersect(existing);
            var named = from b in both join e in existing on b.Hierarchy equals e.Hierarchy where b.Name != e.Name select new { Id = e.ID, ExistingName = e.Name, BstName = b.Name };
            Assert.AreEqual(2, named.Count());
            Assert.AreEqual("New name 1", named.First().BstName);
            Assert.AreEqual("New name 2", named.Last().BstName);
        }

        [Test]
        public void ConvertFromBst()
        {
            var data = new List<ProjectStructure>();
            data.Add(new ProjectStructure
            {
                ProjectCode = ProjectFiller,
                ProjectName = ProjectFiller,
                PhaseCode = PhaseFiller,
                PhaseName = PhaseFiller,
                TaskCode = TaskFiller,
                TaskName = TaskFiller,
                StudioCode = StudioFiller,
                StudioName = StudioFiller
            });
            var g = new ConcurListIntegrator(new ConcurSettings { Environment = "Test", IsDryRun = true }, null);
            var result = g.ConvertBstToConcur(data);
            Assert.IsTrue(result.Count == 4);
            Assert.IsTrue(result[0].ListID == ConcurListIntegrator.ProjectStructureListId);
            Assert.IsTrue(result[1].Name == PhaseFiller);
            Assert.IsTrue(result[3].Level4Code == StudioFiller);
        }
    }
}