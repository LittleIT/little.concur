﻿using System.Threading.Tasks;
using Little.Concur.Expenses.Core.Data;
using RestSharp;
using Serilog;

namespace Little.Concur.Expenses.Core
{
    public class ConcurExpenseIntegrator
    {
        private readonly IConcurSettings _settings;
        private readonly IConcurTokenManager _tokenManager;
        private readonly ILogger _log;
        private readonly RestClient _client;

        public ConcurExpenseIntegrator(IConcurSettings settings, IConcurTokenManager tokenManager)
        {
            _settings = settings;
            _tokenManager = tokenManager;
            _log = Log.ForContext<ConcurExpenseIntegrator>();
            if (!_settings.IsDryRun || !string.IsNullOrWhiteSpace(_settings.ConcurUrl))
            {
                _client = new RestClient(_settings.ConcurUrl);
            }
        }

        //public async Task<ConcurExpenseReports> GetExpenseReportsAsync(string user = "ALL", string nextUrl = null)
        public ConcurExpenseReports GetExpenseReports(string user = "ALL", string nextUrl = null)
        {
            _log.Debug("Retrieving expense reports from Concur API");
            _client.Authenticator = _tokenManager.GetAuthenticator(_client);
            // RestSharp includes the root part of Url in client instance, so it needs to be stripped to avoid invalid request route
            string url = nextUrl.IsEmpty() ? "/api/v3.0/expense/reports" : nextUrl.Replace(_settings.ConcurUrl, "");
            var request = new RestRequest(url, Method.GET);
            request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
            request.AddHeader("Accept", "application/json");
            if (nextUrl.IsEmpty())
            {
                request.AddParameter("user", user);
            }
            var response = _client.Execute<ConcurExpenseReports>(request);
            if (ConcurApiHelper.HasRestError(_log, response) || response.Data == null)
            {
                return null;
            }
            return response.Data;
        }

        public ReceiptImages GetReceiptImages(string user = null, string nextUrl = null)
        {
            _log.Debug("Retrieving expense reports from Concur API");
            _client.Authenticator = _tokenManager.GetAuthenticator(_client);
            // RestSharp includes the root part of Url in client instance, so it needs to be stripped to avoid invalid request route
            string url = nextUrl.IsEmpty() ? "/api/v3.0/expense/receiptimages" : nextUrl.Replace(_settings.ConcurUrl, "");
            var request = new RestRequest(url, Method.GET);
            request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
            request.AddHeader("Accept", "application/json");
            if (nextUrl.IsEmpty() && user.IsNotEmpty())
            {
                request.AddParameter("user", user);
            }
            // limit available; defaults to 25.
            // offset listed as param?
            var response = _client.Execute<ReceiptImages>(request);
            if (ConcurApiHelper.HasRestError(_log, response) || response.Data == null)
            {
                return null;
            }
            return response.Data;
        }

        // Create a new receipt image
        // POST  /api/v3.0/expense/receiptimages
        // Params: user (string:loginid), image (file:required)

        // Append a receipt image
        // PUT  /api/v3.0/expense/receiptimages/{id}
        // Params: id (string:receipt image id), user (string:loginid), image (file:required)

        // DELETE  /api/v3.0/expense/receiptimages/{id}
        // Params: id (string:receipt image id), user (string:loginid)
    }
}