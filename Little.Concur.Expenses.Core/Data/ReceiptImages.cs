﻿using System.Collections.Generic;

namespace Little.Concur.Expenses.Core.Data
{
    public class ReceiptImages
    {
        public List<ReceiptImage> Items { get; set; }
        public string NextPage { get; set; }
    }

    public class ReceiptImage
    {
        public string ID { get; set; }
        public string URI { get; set; }
    }
}