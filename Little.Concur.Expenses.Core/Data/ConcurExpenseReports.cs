﻿using System;
using System.Collections.Generic;

namespace Little.Concur.Expenses.Core.Data
{
    public class ConcurExpenseReports
    {
        public List<ConcurExpenseReport> Items { get; set; }
        public string NextPage { get; set; }
    }

    public class ConcurExpenseReport
    {
        public string Name { get; set; }
        public decimal Total { get; set; }
        public string CurrencyCode { get; set; }
        public string Country { get; set; }
        public string CountrySubdivision { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime SubmitDate { get; set; }
        public DateTime ProcessingPaymentDate { get; set; }
        public DateTime PaidDate { get; set; }
        public bool ReceiptsReceived { get; set; }
        public DateTime UserDefinedDate { get; set; }
        public string LastComment { get; set; }
        public string OwnerLoginID { get; set; }
        public string OwnerName { get; set; }
        public string ApproverLoginID { get; set; }
        public string ApproverName { get; set; }
        public string ApprovalStatusName { get; set; }
        public string ApprovalStatusCode { get; set; }
        public string PaymentStatusName { get; set; }
        public string PaymentStatusCode { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public decimal PersonalAmount { get; set; }
        public decimal AmountDueEmployee { get; set; }
        public decimal AmountDueCompanyCard { get; set; }
        public decimal TotalClaimedAmount { get; set; }
        public decimal TotalApprovedAmount { get; set; }
        public string LedgerName { get; set; }
        public string PolicyID { get; set; }
        public bool EverSentBack { get; set; }
        public bool HasException { get; set; }
        public string WorkflowActionUrl { get; set; }
        public string OrgUnit1 { get; set; }
        public string OrgUnit2 { get; set; }
        public string OrgUnit3 { get; set; }
        public string OrgUnit4 { get; set; }
        public string OrgUnit5 { get; set; }
        public string OrgUnit6 { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }
        public string Custom6 { get; set; }
        public string Custom7 { get; set; }
        public string Custom8 { get; set; }
        public string Custom9 { get; set; }
        public string Custom10 { get; set; }
        public string Custom11 { get; set; }
        public string Custom12 { get; set; }
        public string Custom13 { get; set; }
        public string Custom14 { get; set; }
        public string Custom15 { get; set; }
        public string Custom16 { get; set; }
        public string Custom17 { get; set; }
        public string Custom18 { get; set; }
        public string Custom19 { get; set; }
        public string Custom20 { get; set; }
        public string ID { get; set; }
        public string URI { get; set; }
    }
}