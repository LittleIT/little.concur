﻿namespace Little.Concur
{
    /// <summary>
    /// Commonly used settings for accessing Concur REST APIs
    /// </summary>
    public class ConcurSettings : IConcurSettings
    {
        public string Environment { get; set; }
        public string TestConcurUrl { get; set; }
        public string ProductionConcurUrl { get; set; }
        public string ConcurUsername { get; set; }
        public string ConcurPassword { get; set; }
        public string ConcurConsumerKey { get; set; }
        public string ConcurSecretId { get; set; }
        public bool IsDryRun { get; set; }
        public bool IsTokenFileEnabled { get; set; }

        public string ConcurUrl { get { return Environment == "Production" ? ProductionConcurUrl : TestConcurUrl; } }
    }
}