﻿using System;
using RestSharp.Deserializers;

namespace Little.Concur
{
    public class ConcurAccessToken
    {
        [DeserializeAs(Name = "Instance_Url")]
        public string InstanceUrl { get; set; }

        [DeserializeAs(Name = "Token")]
        public string Token { get; set; }

        [DeserializeAs(Name = "Expiration_date")]
        public DateTime ExpirationDate { get; set; }

        [DeserializeAs(Name = "Refresh_Token")]
        public string RefreshToken { get; set; }
    }
}