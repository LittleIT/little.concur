﻿using System;
using System.IO;
using Newtonsoft.Json;
using RestSharp;
using RestSharp.Authenticators;
using Serilog;

namespace Little.Concur
{
    /// <summary>
    /// Helper class for managing API access tokens. If settings allow, will store token in file system for caching.
    /// Currently utilizes /net2/oauth2 path. May need to be upgraded to newer route soon.
    /// </summary>
    public class ConcurTokenManager : IConcurTokenManager
    {
        public ConcurAccessToken AccessToken { get; set; }
        public string TokenFileName { get; set; } = "api.tkn";

        private readonly ILogger _log;
        private readonly IConcurSettings _settings;

        public ConcurTokenManager(IConcurSettings settings)
        {
            _log = Log.ForContext<ConcurTokenManager>();
            _settings = settings;
        }

        public bool IsValid()
        {
            return AccessToken != null && !string.IsNullOrWhiteSpace(AccessToken.Token) && AccessToken.ExpirationDate > DateTime.Now;
        }

        public void SetupAccessToken(RestClient client)
        {
            if (IsValid())
            {
                _log.Debug("Re-using Concur API access token");
                return;
            }
            if (_settings.IsTokenFileEnabled && File.Exists(TokenFileName))
            {
                _log.Debug("Retrieving existing Concur API access token from file");
                try
                {
                    using (StreamReader file = File.OpenText(TokenFileName))
                    {
                        var serializer = new JsonSerializer();
                        AccessToken = (ConcurAccessToken)serializer.Deserialize(file, typeof(ConcurAccessToken));
                    }
                    if (IsValid())
                    {
                        return;
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(ex, "Unable to load API access token from file");
                }
            }

            _log.Debug("Retrieving new Concur API access token from service");
            // Note: This end point seems to be deprecated. May need to figure out commented code below, or a variation thereof.
            client.Authenticator = new HttpBasicAuthenticator(_settings.ConcurUsername, _settings.ConcurPassword);
            var request = new RestRequest("/net2/oauth2/accesstoken.ashx", Method.GET);
            request.AddHeader("X-ConsumerKey", _settings.ConcurConsumerKey);
            var response = client.Execute<ConcurAccessTokenContainer>(request);
            if (response.StatusCode != System.Net.HttpStatusCode.OK)
            {
                throw new Exception($"Unable to retrieve new Concur API access token: {response.StatusCode} - {response.Content}");
            }
            AccessToken = response.Data.AccessToken;

            //var request = new RestRequest("/oauth2/v0/token", Method.POST);
            //request.AddParameter("client_id", _settings.ConcurConsumerKey);
            //request.AddParameter("client_secret", _settings.ConcurSecretId);
            //request.AddParameter("grant_type", "client_credentials");

            if (_settings.IsTokenFileEnabled)
            {
                using (StreamWriter file = File.CreateText(TokenFileName))
                {
                    var serializer = new JsonSerializer();
                    serializer.Serialize(file, AccessToken);
                }
            }
        }

        public OAuth2AuthorizationRequestHeaderAuthenticator GetAuthenticator(RestClient client)
        {
            SetupAccessToken(client);
            return new OAuth2AuthorizationRequestHeaderAuthenticator(AccessToken.Token);
        }
    }
}