﻿using RestSharp.Deserializers;

namespace Little.Concur
{
    public class ConcurAccessTokenContainer
    {
        [DeserializeAs(Name = "Access_Token")]
        public ConcurAccessToken AccessToken { get; set; }
    }
}