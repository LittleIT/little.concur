﻿namespace Little.Concur
{
    public interface IConcurSettings
    {
        string Environment { get; set; }
        string TestConcurUrl { get; set; }
        string ProductionConcurUrl { get; set; }
        string ConcurUsername { get; set; }
        string ConcurPassword { get; set; }
        string ConcurConsumerKey { get; set; }
        string ConcurSecretId { get; set; }
        bool IsDryRun { get; set; }
        bool IsTokenFileEnabled { get; set; }

        string ConcurUrl { get; }
    }
}