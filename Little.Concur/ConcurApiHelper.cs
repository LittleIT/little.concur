﻿using RestSharp;
using Serilog;

namespace Little.Concur
{
    public static class ConcurApiHelper
    {
        /// <summary>
        /// Since the different API versions handle errors differently, this attempts to be a catch-all. Error states are logged.
        /// </summary>
        /// <returns></returns>
        public static bool HasRestError(ILogger log, IRestResponse response)
        {
            if (response.ErrorException != null || !string.IsNullOrWhiteSpace(response.ErrorMessage))
            {
                if (response.ErrorException != null)
                {
                    log.Error(response.ErrorException, response.ErrorException.Message);
                }
                else
                {
                    log.Error(response.ErrorMessage);
                }
                return true;
            }
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                log.Error("{StatusDescription}: {@Response}", response.StatusDescription, response);
                return true;
            }

            if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
            {
                log.Error("NotFound. Wrong url, most likely. Verify API route. {@Response}", response);
                return true;
            }

            if (response.StatusCode == System.Net.HttpStatusCode.BadRequest)
            {
                log.Error("BadRequest. Probably validation failing. {@Response}", response);
                return true;
            }

            if (response.StatusCode == System.Net.HttpStatusCode.Gone)
            {
                log.Error("Gone. It's not there, so either incorrect param passed or, well, it's gone. {@Response}", response);
                return true;
            }
            
            if (response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
            {
                log.Error("InternalServerError. Something happened on the server. May or may not be caused by what we passed. {@Response}", response);
                return true;
            }
            
            if (response.StatusCode == System.Net.HttpStatusCode.NotAcceptable)
            {
                log.Error("NotAcceptable. You're not using the correct payload (e.g. XML vs. JSON). {@Response}", response);
                return true;
            }
            
            if (response.StatusCode == System.Net.HttpStatusCode.Forbidden)
            {
                log.Error("Forbidden. Server refused request. {@Response}", response);
                return true;
            }
            
            if (response.StatusCode == System.Net.HttpStatusCode.ServiceUnavailable)
            {
                log.Error("ServiceUnavailable. Temporarily down. Try again later. If systemic, verify no caps on API usage. {@Response}", response);
                return true;
            }
            
            if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
            {
                log.Error("Unauthorized. You probably forgot your auth headers. {@Response}", response);
                return true;
            }
            
            if (response.StatusCode == System.Net.HttpStatusCode.MethodNotAllowed)
            {
                log.Error("MethodNotAllowed. Verify with API that you're using correct GET/POST/PUT/DELETE. {@Response}", response);
                return true;
            }

            return false;
        }
    }
}