﻿using RestSharp;
using RestSharp.Authenticators;

namespace Little.Concur
{
    public interface IConcurTokenManager
    {
        ConcurAccessToken AccessToken { get; set; }
        bool IsValid();
        void SetupAccessToken(RestClient client);
        OAuth2AuthorizationRequestHeaderAuthenticator GetAuthenticator(RestClient client);
    }
}