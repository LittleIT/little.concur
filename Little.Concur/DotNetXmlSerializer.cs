﻿using System.IO;
using RestSharp.Serializers;
using Serilog;
using Serilog.Events;

namespace Little.Concur
{
    /// <summary>
    /// The baked-in RestSharp XML serializer is opinionated. This is a simple way to override it with basic .NET System.Xml functionality,
    /// along with including logging to needed event level.
    /// </summary>
    public class DotNetXmlSerializer : ISerializer
    {
        public string RootElement { get; set; }
        public string Namespace { get; set; }
        public string DateFormat { get; set; }
        public string ContentType { get; set; }

        private readonly ILogger _log;
        private readonly LogEventLevel _loglevel;

        public DotNetXmlSerializer(ILogger log, LogEventLevel loglevel = LogEventLevel.Debug)
        {
            _log = log;
            _loglevel = loglevel;
            ContentType = "text/xml";
        }

        public string Serialize(object obj)
        {
            string xml;
            using (TextWriter writer = new StringWriter())
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(obj.GetType());
                serializer.Serialize(writer, obj);
                xml = writer.ToString();
            }
            _log.Write(_loglevel, "Serialized to {@Xml}", xml);
            return xml;
        }
    }
}