﻿using System;

namespace Little.Concur
{
    public static class DateTimeExtensions
    {
        public static int? SafeYear(this DateTime? date)
        {
            if (!date.HasValue)
                return null;
            return date.Value.Year;
        }

        public static string SafeShortDate(this DateTime? date)
        {
            if (!date.HasValue)
                return null;
            return date.Value.ToString("yyyy-MM-dd");
        }
    }
}