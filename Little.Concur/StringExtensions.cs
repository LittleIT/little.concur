﻿using System;

namespace Little.Concur
{
    public static class StringExtensions
    {
        /// <summary>
        /// Safe comparison function even if either value is null. Null and blank strings are considered the same.
        /// </summary>
        public static bool SafeEquals(this string source, string other, StringComparison comp = StringComparison.OrdinalIgnoreCase)
        {
            if (string.IsNullOrEmpty(source))
            {
                return string.IsNullOrEmpty(other);
            }
            return source.Equals(other, comp);
        }

        /// <summary>
        /// Safe hash function even if source is null. Null and blank strings are considered the same.
        /// </summary>
        public static int SafeHashCode(this string source)
        {
            return source == null ? "".GetHashCode() : source.GetHashCode();
        }

        /// <summary>
        /// Convenience method for performing fluent is empty check
        /// </summary>
        public static bool IsEmpty(this string val)
        {
            return string.IsNullOrWhiteSpace(val);
        }

        /// <summary>
        /// Convenience method for performing fluent is not empty check
        /// </summary>
        public static bool IsNotEmpty(this string val)
        {
            return !string.IsNullOrWhiteSpace(val);
        }

        public static string SafeTrim(this string source)
        {
            return string.IsNullOrWhiteSpace(source) ? null : source.Trim();
        }
    }
}