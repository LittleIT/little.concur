﻿var PaymentBatch = PaymentBatch ? PaymentBatch : function () {
    return {
        init: function () {
            $("form.ajax").submit(Utilities.AjaxSubmit);
            $(document).on("click", ".ForceRefresh", PaymentBatch.ForceRefresh);
            $(document).on("click", ".CloseConcurBatch", PaymentBatch.CloseConcurBatch);
            $(document).on("click", ".StageBatch", PaymentBatch.StageBatch);
            $(document).on("click", ".SaveBatchBst", PaymentBatch.SaveBatchBst);
        },

        ForceRefresh: function () {
            $.ajax({
                type: 'GET',
                url: '/api/PaymentBatchAPI/ForceRefresh/',
                dataType: "json"
            })
            .done(Utilities.AjaxSuccess)
            .fail(Utilities.AjaxError);
        },

        CloseConcurBatch: function () {
            var id = $(this).closest(".BatchSummary").data("batchid");
            $.ajax({
                type: 'GET',
                url: '/api/PaymentBatchAPI/CloseConcurBatch/' + encodeURIComponent(id),
                dataType: "json"
            })
            .done(Utilities.AjaxSuccess)
            .fail(Utilities.AjaxError);
        },

        StageBatch: function () {
            var id = $(this).closest(".BatchSummary").data("batchid");
            $.ajax({
                type: 'GET',
                url: '/api/PaymentBatchAPI/Stage/' + encodeURIComponent(id),
                dataType: "json"
            })
            .done(Utilities.AjaxSuccess)
            .fail(Utilities.AjaxError);
        },

        SaveBatchBst: function () {
            var id = $(this).closest(".BatchSummary").data("batchid");
            $.ajax({
                type: 'GET',
                url: '/api/PaymentBatchAPI/SaveToBst/' + encodeURIComponent(id),
                dataType: "json"
            })
            .done(Utilities.AjaxSuccess)
            .fail(Utilities.AjaxError);
        }
    }
}();

$(function () {
    PaymentBatch.init();
});