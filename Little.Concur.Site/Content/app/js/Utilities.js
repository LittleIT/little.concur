﻿var Utilities = Utilities ? Utilities : function () {
    return {
        Log: function (sMsg) {
            if (typeof console != "undefined" && typeof console.debug != "undefined") {
                console.log(sMsg);
            }
        },

        ConvertToInt: function (number) {
            number += "";
            return Number(Number(number.replace(/(-)[^\d.]/g, '')).toFixed(0));
        },

        ConvertToDecimal: function (number, iDecimalPlaces) {
            number += "";
            number = Number(number.replace(/(-)[\,\$\%]/g, ''));
            if (iDecimalPlaces) {
                number = number.toFixed(iDecimalPlaces);
            }
            return Number(number);
        },

        FormatNumber: function (number, iDecimalPlaces) {
            number = Number(number).toFixed(iDecimalPlaces);
            var x = number.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        },

        GetMessageContainer: function (e, childFilter, rootFilter) {
            var $msg = $(e).find(childFilter);
            if ($msg.length == 0) {
                return $(rootFilter);
            }
            return $msg;
        },

        AjaxSubmit: function (e) {
            e.preventDefault();
            var $form = $(this);
            var $msg = Utilities.GetMessageContainer(this, ".alert-success", "#pageMessage");
            var $error = Utilities.GetMessageContainer(this, ".alert-danger", "#pageError");
            var $submitButton = $form.find(".submitButton");
            $("input.error, textarea.error, select.error").removeClass("error");
            $(".form-group").removeClass("has-error");
            $error.hide();

            var formdata = $form.data("simple_serialize") ? $form.serialize() : JSON.stringify($form.serializeJSON({ useIntKeysAsArrayIndex: true }));

            var msg = $form.data("submit_message");
            if (msg) {
                $msg.empty().append(msg).fadeIn('fast');
                $form.find(".loadingAnimation").fadeIn();
            }
            else {
                $submitButton.button('loading');
            }
            $.ajax({
                url: $form.attr("action"),
                dataType: "json",
                method: $form.attr("method"),
                data: formdata,
                contentType: $form.data("simple_serialize") ? "application/x-www-form-urlencoded; charset=UTF-8" : "application/json",
                context: this
            })
            .done(Utilities.AjaxSuccess)
            .fail(Utilities.AjaxError);
        },

        ShowMessageLink: function ($msgContainer, message, url, islinkonly) {
            if (islinkonly) {
                $msgContainer.html(message + " <a class='alert-link' href='" + url + "'>Continue</a>").fadeIn('fast');
            }
            else {
                setTimeout(function () { window.location.href = url; }, 1000);
            }
        },

        AjaxSuccess: function (result) {
            var $form = $(this);
            $form.find(".submitButton").button('reset');
            $form.find(".loadingAnimation").fadeOut();
            var $msg = Utilities.GetMessageContainer(this, ".alert-success", "#pageMessage");
            var $error = Utilities.GetMessageContainer(this, ".alert-danger", "#pageError");

            if (result.success) {
                var sMsg = result.message ? result.message : "Save successful";
                if (result.successurl) {
                    Utilities.ShowMessageLink($msg, sMsg, result.successurl, result.islinkonly);
                }
                else if ($form.data("successurl")) {
                    Utilities.ShowMessageLink($msg, sMsg, $form.data("successurl"), $form.data("islinkonly"));
                }
                else {
                    $msg.html(sMsg).fadeIn('fast');
                }
            }
            else {
                $msg.fadeOut('fast');
                if (result.errors && result.errors.length > 0) {
                    $error.empty();
                    var errorlist = "<span>Save Failed:</span><ul class='errors'>";
                    for (var i = 0; i < result.errors.length; i++) {
                        errorlist += "<li>" + result.errors[i]["message"] + "</li>";
                        $("#" + result.errors[i]["field"]).addClass("error");
                        $("#" + result.errors[i]["field"]).closest(".form-group").addClass("has-error");
                    }
                    errorlist += "</ul>";
                    $error.append(errorlist);
                } else if (result.message) {
                    $error.text(result.message);
                } else {
                    $error.text("Sorry, you've just experienced an Unknown Server Error.");
                }
                $error.show();
            }
        },

        AjaxError: function (XMLHttpRequest, textStatus, errorThrown) {
            var $form = $(this);
            $form.find(".submitButton").button('reset');
            $form.find(".loadingAnimation").fadeOut();

            var $msg = Utilities.GetMessageContainer(this, ".alert-success", "#pageMessage");
            var $error = Utilities.GetMessageContainer(this, ".alert-danger", "#pageError");

            var msg = "Server error";
            var result = XMLHttpRequest.responseJSON;
            if (result && result.message) {
                msg = result.message;
            }
            $msg.fadeOut('fast');
            $error.html(msg).fadeIn('fast');
        }
    }
}();