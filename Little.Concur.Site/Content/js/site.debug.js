var Utilities = Utilities ? Utilities : function () {
    return {
        Log: function (sMsg) {
            if (typeof console != "undefined" && typeof console.debug != "undefined") {
                console.log(sMsg);
            }
        },

        ConvertToInt: function (number) {
            number += "";
            return Number(Number(number.replace(/(-)[^\d.]/g, '')).toFixed(0));
        },

        ConvertToDecimal: function (number, iDecimalPlaces) {
            number += "";
            number = Number(number.replace(/(-)[\,\$\%]/g, ''));
            if (iDecimalPlaces) {
                number = number.toFixed(iDecimalPlaces);
            }
            return Number(number);
        },

        FormatNumber: function (number, iDecimalPlaces) {
            number = Number(number).toFixed(iDecimalPlaces);
            var x = number.split('.');
            var x1 = x[0];
            var x2 = x.length > 1 ? '.' + x[1] : '';
            var rgx = /(\d+)(\d{3})/;
            while (rgx.test(x1)) {
                x1 = x1.replace(rgx, '$1' + ',' + '$2');
            }
            return x1 + x2;
        },

        GetMessageContainer: function (e, childFilter, rootFilter) {
            var $msg = $(e).find(childFilter);
            if ($msg.length == 0) {
                return $(rootFilter);
            }
            return $msg;
        },

        AjaxSubmit: function (e) {
            e.preventDefault();
            var $form = $(this);
            var $msg = Utilities.GetMessageContainer(this, ".alert-success", "#pageMessage");
            var $error = Utilities.GetMessageContainer(this, ".alert-danger", "#pageError");
            var $submitButton = $form.find(".submitButton");
            $("input.error, textarea.error, select.error").removeClass("error");
            $(".form-group").removeClass("has-error");
            $error.hide();

            var formdata = $form.data("simple_serialize") ? $form.serialize() : JSON.stringify($form.serializeJSON({ useIntKeysAsArrayIndex: true }));

            var msg = $form.data("submit_message");
            if (msg) {
                $msg.empty().append(msg).fadeIn('fast');
                $form.find(".loadingAnimation").fadeIn();
            }
            else {
                $submitButton.button('loading');
            }
            $.ajax({
                url: $form.attr("action"),
                dataType: "json",
                method: $form.attr("method"),
                data: formdata,
                contentType: $form.data("simple_serialize") ? "application/x-www-form-urlencoded; charset=UTF-8" : "application/json",
                context: this
            })
            .done(Utilities.AjaxSuccess)
            .fail(Utilities.AjaxError);
        },

        ShowMessageLink: function ($msgContainer, message, url, islinkonly) {
            if (islinkonly) {
                $msgContainer.html(message + " <a class='alert-link' href='" + url + "'>Continue</a>").fadeIn('fast');
            }
            else {
                setTimeout(function () { window.location.href = url; }, 1000);
            }
        },

        AjaxSuccess: function (result) {
            var $form = $(this);
            $form.find(".submitButton").button('reset');
            $form.find(".loadingAnimation").fadeOut();
            var $msg = Utilities.GetMessageContainer(this, ".alert-success", "#pageMessage");
            var $error = Utilities.GetMessageContainer(this, ".alert-danger", "#pageError");

            if (result.success) {
                var sMsg = result.message ? result.message : "Save successful";
                if (result.successurl) {
                    Utilities.ShowMessageLink($msg, sMsg, result.successurl, result.islinkonly);
                }
                else if ($form.data("successurl")) {
                    Utilities.ShowMessageLink($msg, sMsg, $form.data("successurl"), $form.data("islinkonly"));
                }
                else {
                    $msg.html(sMsg).fadeIn('fast');
                }
            }
            else {
                $msg.fadeOut('fast');
                if (result.errors && result.errors.length > 0) {
                    $error.empty();
                    var errorlist = "<span>Save Failed:</span><ul class='errors'>";
                    for (var i = 0; i < result.errors.length; i++) {
                        errorlist += "<li>" + result.errors[i]["message"] + "</li>";
                        $("#" + result.errors[i]["field"]).addClass("error");
                        $("#" + result.errors[i]["field"]).closest(".form-group").addClass("has-error");
                    }
                    errorlist += "</ul>";
                    $error.append(errorlist);
                } else if (result.message) {
                    $error.text(result.message);
                } else {
                    $error.text("Sorry, you've just experienced an Unknown Server Error.");
                }
                $error.show();
            }
        },

        AjaxError: function (XMLHttpRequest, textStatus, errorThrown) {
            var $form = $(this);
            $form.find(".submitButton").button('reset');
            $form.find(".loadingAnimation").fadeOut();

            var $msg = Utilities.GetMessageContainer(this, ".alert-success", "#pageMessage");
            var $error = Utilities.GetMessageContainer(this, ".alert-danger", "#pageError");

            var msg = "Server error";
            var result = XMLHttpRequest.responseJSON;
            if (result && result.message) {
                msg = result.message;
            }
            $msg.fadeOut('fast');
            $error.html(msg).fadeIn('fast');
        }
    }
}();
var WetzTables = WetzTables ? WetzTables : function () {
    return {
        init: function () {
            $("table.datatable").each(function () {
                var $table = $(this);
                var colDefs = [];
                var columns = [];
                $(this).find("thead th").each(function (i) {
                    var fieldData = $(this).data("data");
                    var fieldName = $(this).data("name")
                    if (!fieldName) {
                        fieldName = fieldData
                    }
                    columns.push({ "data": fieldData, "name": fieldName, "searchable": $(this).data("searchable") !== false });
                    var renderHelper = $(this).data("renderhelper");
                    switch (renderHelper) {
                        case "renderLink":
                            colDefs.push({ "targets": i, "render": WetzTables.renderLink });
                            break;
                        case "renderLinkView":
                            colDefs.push({ "targets": i, "render": WetzTables.renderLinkView });
                            break;
                        case "renderTrimmer":
                            colDefs.push({ "targets": i, "render": WetzTables.renderTrimmer });
                            break;
                        case "renderDateYear":
                            colDefs.push({ "targets": i, "render": WetzTables.renderDateYear });
                            break;
                        case "renderDecimalTens":
                            colDefs.push({ "targets": i, "render": WetzTables.renderDecimalTens });
                            break;
                        case "renderDecimal":
                            colDefs.push({ "targets": i, "render": WetzTables.renderDecimal });
                            break;
                        case "renderDecimalThousands":
                            colDefs.push({ "targets": i, "render": WetzTables.renderDecimalThousands });
                            break;
                    }
                });
                if (!$table.hasClass("serverside")) {
                    if (colDefs.length === 0) {
                        $table.dataTable();
                    }
                    else {
                        $table.dataTable({ "processing": true, "columnDefs": colDefs });
                    }
                } else {
                    $table.dataTable({
                        "processing": true,
                        "serverSide": true,
                        "columnDefs": colDefs,
                        "columns": columns
                    });
                    var table = $table.DataTable();
                    table.columns().every(WetzTables.DataTableColumnSearch);
                }
            });
        },

        renderLink: function (data, type, row) {
            if (row.url == null) { return data; }
            return '<a href="' + row.url + '">' + data + '</a>';
        },

        renderLinkView: function (data, type, row) {
            if (data == null) { return ""; }
            return '<a href="' + data + '">View</a>';
        },

        renderTrimmer: function (data, type, row) {
            if (data == null) { return ""; }
            return data.length > 50 ? data.substr(0, 48) + '...' : data;
        },

        renderDateYear: function (data, type, row) {
            if (data == null) { return ""; }
            return data.length > 10 ? data.substr(0, 10) : data;
        },

        renderDecimalTens: function (data, type, row) {
            if (data == null) { return ""; }
            return Utilities.FormatNumber(data, 1);
        },

        renderDecimal: function (data, type, row) {
            if (data == null) { return ""; }
            return Utilities.FormatNumber(data, 2);
        },

        renderDecimalThousands: function (data, type, row) {
            if (data == null) { return ""; }
            return Utilities.FormatNumber(data, 3);
        },

        DataTableColumnSearch: function () {
            var c = this;
            $("input", this.footer()).on("keyup change", function () {
                if (c.search() !== this.value) {
                    c.search(this.value).draw();
                }
            });
        }
    }
}();

$(function () {
    WetzTables.init();
});
var PaymentBatch = PaymentBatch ? PaymentBatch : function () {
    return {
        init: function () {
            $("form.ajax").submit(Utilities.AjaxSubmit);
            $(document).on("click", ".ForceRefresh", PaymentBatch.ForceRefresh);
            $(document).on("click", ".CloseConcurBatch", PaymentBatch.CloseConcurBatch);
            $(document).on("click", ".StageBatch", PaymentBatch.StageBatch);
            $(document).on("click", ".SaveBatchBst", PaymentBatch.SaveBatchBst);
        },

        ForceRefresh: function () {
            $.ajax({
                type: 'GET',
                url: '/api/PaymentBatchAPI/ForceRefresh/',
                dataType: "json"
            })
            .done(Utilities.AjaxSuccess)
            .fail(Utilities.AjaxError);
        },

        CloseConcurBatch: function () {
            var id = $(this).closest(".BatchSummary").data("batchid");
            $.ajax({
                type: 'GET',
                url: '/api/PaymentBatchAPI/CloseConcurBatch/' + encodeURIComponent(id),
                dataType: "json"
            })
            .done(Utilities.AjaxSuccess)
            .fail(Utilities.AjaxError);
        },

        StageBatch: function () {
            var id = $(this).closest(".BatchSummary").data("batchid");
            $.ajax({
                type: 'GET',
                url: '/api/PaymentBatchAPI/Stage/' + encodeURIComponent(id),
                dataType: "json"
            })
            .done(Utilities.AjaxSuccess)
            .fail(Utilities.AjaxError);
        },

        SaveBatchBst: function () {
            var id = $(this).closest(".BatchSummary").data("batchid");
            $.ajax({
                type: 'GET',
                url: '/api/PaymentBatchAPI/SaveToBst/' + encodeURIComponent(id),
                dataType: "json"
            })
            .done(Utilities.AjaxSuccess)
            .fail(Utilities.AjaxError);
        }
    }
}();

$(function () {
    PaymentBatch.init();
});