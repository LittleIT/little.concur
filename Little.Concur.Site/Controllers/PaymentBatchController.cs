﻿using System.Threading.Tasks;
using System.Web.Mvc;
using Little.Concur.PaymentBatch.Core;
using Serilog;

namespace Little.Concur.Site.Controllers
{
    public class PaymentBatchController : Controller
    {
        private readonly ILogger _log;
        private readonly ConcurPaymentBatchIntegrator _integrator;

        public PaymentBatchController(IConcurPaymentBatchIntegrator integrator) : base()
        {
            _integrator = (ConcurPaymentBatchIntegrator)integrator;
            _log = Log.ForContext<PaymentBatchController>();
        }

        // GET: PaymentBatch
        public async Task<ActionResult> Index()
        {
            var model = await _integrator.GetStagingBatchSummariesAsync();
            return View("index", model);
        }

        // GET: PaymentBatch/Details/5
        public async Task<ActionResult> Details(string id)
        {
            var model = await _integrator.GetStagingBatchSummaryAsync(id);
            return View("details", model);
        }
    }
}
