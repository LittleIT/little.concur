﻿using System;
using System.Web.Http;

namespace Little.Concur.Site.Controllers.API
{
    public class PingController : ApiController
    {
        public PingController() : base()
        {
        }

        // GET: api/PingAPI
        public DateTimeOffset Get()
        {
            return DateTimeOffset.Now;
        }
    }
}