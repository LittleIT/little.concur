﻿using System.Threading.Tasks;
using System.Web.Http;
using Little.Concur.PaymentBatch.Core;
using Little.Concur.Site.Models;
using Serilog;

namespace Little.Concur.Site.Controllers.API
{
    public class PaymentBatchAPIController : ApiController
    {
        private readonly ILogger _log;
        private readonly ConcurPaymentBatchIntegrator _integrator;

        public PaymentBatchAPIController(IConcurPaymentBatchIntegrator integrator) : base()
        {
            _integrator = (ConcurPaymentBatchIntegrator)integrator;
            _log = Log.ForContext<PaymentBatchAPIController>();
        }

        [HttpGet]
        [ActionName("ForceRefresh")]
        public async Task<AjaxActionModel> RefreshStagingBatchSummaryAsync()
        {
            await _integrator.RefreshStagingBatchSummaryAsync();
            return new AjaxActionModel
            {
                success = true,
                successurl = "/PaymentBatch"
            };
        }

        [HttpGet]
        [ActionName("CloseConcurBatch")]
        public async Task<AjaxActionModel> CloseConcurBatchAsync(string id)
        {
            if (id.IsEmpty())
            {
                return new AjaxActionModel
                {
                    success = false,
                    message = "Batch ID missing"
                };
            }
            var batch = await _integrator.GetStagingBatchSummaryAsync(id);
            if (batch == null)
            {
                return new AjaxActionModel
                {
                    success = false,
                    message = $"Unable to find record for batch id: {id}"
                };
            }
            var filePath = _integrator.CloseAndSaveBatchFile(batch);
            if (filePath.IsEmpty())
            {
                return new AjaxActionModel
                {
                    success = false,
                    message = $"Unable to save file for batch id: {id}"
                };
            }
            return new AjaxActionModel
            {
                success = true
            };
        }

        [HttpGet]
        [ActionName("Stage")]
        public async Task<AjaxActionModel> Stage(string id)
        {
            if (id.IsEmpty())
            {
                return new AjaxActionModel
                {
                    success = false,
                    message = "Batch ID missing"
                };
            }
            var batch = await _integrator.GetStagingBatchSummaryAsync(id);
            if (batch == null)
            {
                return new AjaxActionModel
                {
                    success = false,
                    message = $"Unable to find record for batch id: {id}"
                };
            }
            var count = await _integrator.StageAsync(batch);
            return new AjaxActionModel
            {
                success = count > 0,
                message = count <= 0 ? "Unable to stage journal entries" : $"Staged {count} journal entries"
            };
        }

        [HttpGet]
        [ActionName("SaveToBst")]
        public async Task<AjaxActionModel> SaveToBst(string id)
        {
            if (id.IsEmpty())
            {
                return new AjaxActionModel
                {
                    success = false,
                    message = "Batch ID missing"
                };
            }
            await _integrator.SaveBatchToBst(id);
            return new AjaxActionModel
            {
                success = true
            };
        }
    }
}