﻿namespace Little.Concur.Site.Models
{
    public class AjaxActionModel
    {
        public bool success { get; set; }
        public string message { get; set; }
        public string successurl { get; set; }
        public bool islinkonly { get; set; }
        public object data { get; set; }
        //public IEnumerable<AjaxError> errors { get; set; } // message, field
    }
}