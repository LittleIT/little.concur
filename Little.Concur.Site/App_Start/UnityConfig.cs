using System;
using Little.Concur.PaymentBatch.Core;
using Microsoft.Extensions.Configuration;
using Serilog;
using Serilog.Events;
using SerilogWeb.Classic;
using SerilogWeb.Classic.Enrichers;
using SerilogWeb.Classic.WebApi.Enrichers;
using Unity;

namespace Little.Concur.Site
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        private static Lazy<IUnityContainer> container =
            new Lazy<IUnityContainer>(() =>
            {
                var container = new UnityContainer();
                RegisterTypes(container);
                return container;
            });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");
            var config = builder.Build();
            var settings = new ConcurBatchSettings();
            config.GetSection("ConcurSettings").Bind(settings);
            container.RegisterInstance<IConfiguration>(config);
            container.RegisterInstance<IConcurSettings>(settings);
            container.RegisterSingleton<IConcurTokenManager, ConcurTokenManager>();
            container.RegisterType<IConcurPaymentBatchIntegrator, ConcurPaymentBatchIntegrator>();
            ApplicationLifecycleModule.RequestLoggingLevel = LogEventLevel.Debug;
            ApplicationLifecycleModule.LogPostedFormData = LogPostedFormDataOption.OnlyOnError;
            string loggingConnection = config.GetConnectionString("LoggingConnection");
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .Enrich.WithMachineName()
                .Enrich.WithEnvironmentUserName()
                .Enrich.With<WebApiRouteTemplateEnricher>()
                .Enrich.With<WebApiRouteDataEnricher>()
                .Enrich.With<WebApiControllerNameEnricher>()
                .Enrich.With<WebApiActionNameEnricher>()
                .Enrich.With<HttpRequestUserAgentEnricher>()
                .Enrich.With<UserNameEnricher>()
                .WriteTo.MSSqlServer(loggingConnection, "Logs", LogEventLevel.Information)
                .CreateLogger();
        }
    }
}