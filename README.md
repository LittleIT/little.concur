# Little.Concur #

Includes all projects related to Concur API integration. General naming scheme is Little.Concur.[name].[type], where type is "Console" for command line apps, "Core" for shared libraries, "Site" for web sites. Console applications are just structural code; all logic for them is in the shared library. Everything targets the current .NET Framework, 4.7.x.

## Projects ##

* Database - all database scripts
* Little.Concur - Shared library with common API functionality
* Little.Concur.Lists.[x] - Synchronize project structure data from the source BST system via the Concur APIs
* Little.Concur.Employees.[x] - Synchronize employee data from the source BST system via the Concur APIs
* Little.Concur.Expenses.Core - Integration business logic between BST and Concur APIs
* Little.Concur.PaymentBatch.[x] - Import payment batch data from the source Concur APIs to the BST system via interface tables
* Little.Concur.Test - Unit tests for shared libraries
* Little.Concur.Site - Custom web site for providing UI for any Concur integration.

## Settings ##

The applications are configured using a "appsettings.json" file. They all use the same core settings; any additional settings for a specific app will be noted below.

* ConnectionStrings
    * TestConnection - BST test database
    * ProductionConnection - BST production database
    * LoggingConnection - Logging database
* ConcurSettings
    * Environment - Test/Production
    * TestConcurUrl - Concur API Url to use for testing
    * ProductionConcurUrl - Concur API Url to use for production
    * ConcurUsername - Concur API user
    * ConcurPassword - Concur API password
    * ConcurConsumerKey - Concur API consumer key
    * ConcurSecretId - Concur API secret id
    * IsDryRun - If true, no data is saved to any system (excluding logging)
    * BatchFolderPath - Little.Concur.PaymentBatch setting; fully qualified directory path to save batch files

## Logging ##

[Serilog](https://serilog.net/) is used for logging. It currently logs information/warning/error events to the console and to a new database called COMMON (debug events are only displayed locally in debug window). You can query against the table Logs or ViewLogs, which pulls out some metadata for easier viewing.

    select top 20 * from ViewLogs order by id desc

## Web Site ##

### Architecture ###

The web application utilizes a combination of ASP.NET MVC and WebApi for back end services, hosted on IIS. The MVC server-side views are rendered using Razor. AJAX calls utilize WebApi. Unity is used for IoC. All database calls use Dapper. Concur API calls use the same shared library as the console applications.

Client libraries are maintained with a combination of NPM (for defining, retrieving, and updating) and Gulp (for moving the appropriate files to their needed location, as well as minifying/combining assets). NPM will only be executed when triggered (either manually or by changing the package.json file). Gulp will automatically minify/combine files in Content\app\css+js on every build in Visual Studio. Vendor libraries will need to be manually triggered (instructions below).

Client Libraries:

* jQuery (latest)
* Bootstrap v4 - UI framework. Includes PopperJs for popups, etc.
* FontAwesome - useful icons
* DataTables - client-side dynamic tables (sorting, filtering, exporting); requires data set with known bounds
* jsRender - client-side rendering
* jQuerySerialization - client-side AJAX helper

## How to build

### Requirements
* You'll need the latest Visual Studio 2017 (any edition), although it's possible other modern tools will also work (VS Code, Rider)
* You'll need 4.7.1+ installed, on build machine and target server
* You'll need NPM installed on build machine

### Updating .NET Packages

While you can update Nuget manually for each project, it's easiest to simply use a command to update the entire solution. Obviously this may cause breaking changes in your code, as libraries evolve over time.

* Go to Package Manager Console (View menu > Other Windows > Package Manager Console)
* Run "Update-Package" (no quotes)

### Updating Client-side Packages

* Open a command line in the web project folder
* Run "npm update --dev" (no quotes)
* After it's done, right click on "packages.json" and click "Restore Packages"
* Open Task Runner Explorer (right click on "gulpfile.js")
* Right click on "vendor" and run

### Migrating to Server

It's probably easiest to just use the Publish functionality baked into Visual Studio.
